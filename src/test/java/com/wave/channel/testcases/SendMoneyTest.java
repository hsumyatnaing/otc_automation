package com.wave.channel.testcases;

import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.pages.HomeScreen;
import com.wave.channel.pages.SendMoneyScreen;
import com.wave.channel.util.TestUtil;

public class SendMoneyTest extends TestBase {

	@BeforeClass(alwaysRun=false)
	public void setupClass() {		
		initialize();	
		TestUtil.loginApp();
	}

	// Old Version 1.38 (Auto secret code)
//	@Test(priority=0, enabled=true, groups = {"sanity","regression","testpartial"})
//	public void SendMoneyOTC_Success() {
//
//		SendMoneyScreen sm = new SendMoneyScreen();
//		HomeScreen HomeScr = new HomeScreen();
////		PinScreen pinS = new PinScreen();
//		
//		try {
//            TestUtil.sleepTime(1000);            
//			String bef_bal=HomeScr.Get_Balance();	// get before wallet balance	
//			
//			sm = HomeScr.goToSendMoney(); // open SendMoney screen			
//            sm.fill_data_OTC(sm.senderPhno, sm.senderName, sm.senderNRC, sm.OTCPhno, sm.receiverName, sm.receiverNRC, sm.amount); // fill data in each field
//						
//			sm.click_Continue(); // click Continue button
//			sm.click_Confirm(); // click Confirm button
//			TestUtil.visibilityEleWaitByLocator(sm.btnFinish);	// waiting time to see success screen
//			String actual_result = sm.get_result();	// get result
//			TestUtil.addSS2Report("SendMoneyOTC_Success");	// take SS and add to report
//			Assert.assertEquals(actual_result, sm.expectedResult_Success);	// compare the expected result and actual result
//			TestUtil.scroll2Element("Date"); // scroll down			
//			tid_SendMoney2OTC = TestUtil.get_TID(sm.txt_Summary,sm.indexOTC);  // keep TID for receive Money script            			
//			TestUtil.Click_BillSuccess(); // click button to go back to Home page
//			String aft_bal=HomeScr.Get_Balance(); // get after wallet balance
//			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, sm.amount ,sm.fee ,sm.com); // balance deduction calculation
//			
//		} catch(NoSuchElementException e) {
//			
//			test.log(Status.SKIP, e.getMessage());
//			TestUtil.no_element_err();
//			
//		} catch(AssertionError e) {
//			
//			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
//			TestUtil.Return_HomeScreen();
//		}
//	}

// Version 1.3.9 (Manual Secret Code )
	@Test(priority=0, enabled=true, groups = {"sanity","regression"})
	public void SendMoneyOTC_Success() {

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();
		
		try {
            TestUtil.sleepTime(1000);            
			String bef_bal=HomeScr.Get_Balance();
			sm = HomeScr.goToSendMoney(); 		
			sm.fill_data_OTC(sm.senderPhno, sm.senderName, sm.senderNRC, sm.OTCPhno, sm.receiverName, sm.receiverNRC, sm.amount); // fill data in each field
			sm.click_Continue(); // click Continue button
			sm.click_Confirm(); // click Confirm button
			TestUtil.visibilityEleWaitByLocator(sm.btnFinish);	// waiting time to see success screen
			String actual_result = sm.get_result();	// get result
			TestUtil.addSS2Report("SendMoneyOTC_Success");	// take SS and add to report
			Assert.assertEquals(actual_result, sm.expectedResult_Success);	// compare the expected result and actual result
			TestUtil.scroll2Element("Date"); // scroll down			
			tid_SendMoney2OTC = TestUtil.get_TID(sm.txt_Summary,sm.indexOTC);  // keep TID for receive Money script
			
			TestUtil.Click_Success(); // click button to go back to Home page
			String aft_bal=HomeScr.Get_Balance(); // get after wallet balance
			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, sm.amount ,sm.fee ,sm.com); // balance deduction calculation
		
		} catch(NoSuchElementException e) {
			
			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();
			
		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}
	
		
	@Test(priority=1, enabled=true, groups = {"sanity","regression"})
	public void SendMoneyMA_Success() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();
		
		try {			
			TestUtil.sleepTime(1000);            
			String bef_bal=HomeScr.Get_Balance();	// get before balance
			sm = HomeScr.goToSendMoney(); // open Send Money screen
			sm.open_MAtab(); // open To MA tab
			sm.fill_data_MA(sm.senderPhno,sm.senderName,sm.senderNRC,sm.MAPhno,sm.amount); // fill data
			sm.click_Continue(); // click Continue
			sm.click_Confirm(); // click Confirm
			TestUtil.visibilityEleWaitByLocator(sm.btnFinish); // waiting time to see success screen
			String actual_result = sm.get_result(); // get actual result
			TestUtil.addSS2Report("SendMoneyMA_Success"); // take SS and add to report
			Assert.assertEquals(actual_result, sm.expectedResult_Success); // compare result				
			TestUtil.scroll2Element("Date"); // scroll down
			TestUtil.get_TID(sm.txt_Summary,sm.indexMA); // get TID and add to report
			TestUtil.Click_Success(); // click button to go back to Home page
			String aft_bal=HomeScr.Get_Balance(); // get after balance
			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, sm.amount ,sm.fee ,sm.com);// calculate balance deduction	

		} catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());	
			TestUtil.no_element_err();
			
		} catch(AssertionError e) {	
			
			test.log(Status.FAIL, e.getMessage());
//			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();
			
		}
	}


	// ------------------ Send Money to OTC Failure Test Cases -----------------------------
	@Test(priority=2, enabled=true, groups = {"regression"})
	public void SendMoneyOTC_Fail_SenderPartner() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {	
			TestUtil.sleepTime(1000);   
			sm = HomeScr.goToSendMoney(); 
			// fill Partner Phone Number in Sender field
			sm.fill_data_OTC(sm.partnerPhno, sm.senderName, sm.senderNRC, sm.OTCPhno, sm.receiverName, sm.receiverNRC, sm.amount);
			sm.click_Continue(); 
			sm.click_Confirm();			
			String actual_result = sm.get_result();	// get result	
			TestUtil.addSS2Report("SendMoneyOTC_Fail_SenderPartner"); 
			Assert.assertEquals(actual_result, sm.err_sender); 
			TestUtil.sleepTime(1000);
			TestUtil.click_TryAgain(); // click Try again button to go back to Send Money screen
			TestUtil.Return_HomeScreen(); // go back to Home Screen

		} catch(NoSuchElementException e) {
			test.log(Status.SKIP, e.getMessage());			
		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}


	@Test(priority=3, enabled=true, groups = {"regression"})
	public void SendMoneyOTC_Fail_ReceiverPartner() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {	
			
			TestUtil.sleepTime(1000);
			sm = HomeScr.goToSendMoney();
			// fill Partner Phone Number in Receiver field
			sm.fill_data_OTC(sm.senderPhno, sm.senderName, sm.senderNRC, sm.partnerPhno, sm.receiverName, sm.receiverNRC, sm.amount);
			sm.click_Continue();
			sm.click_Confirm();			
			String actual_result = sm.get_result();	
			TestUtil.addSS2Report("SendMoneyOTC_Fail_ReceiverPartner");	
			Assert.assertEquals(actual_result, sm.err_receiver);	
			TestUtil.sleepTime(1000);	
			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();

		} catch(NoSuchElementException e) {
			test.log(Status.SKIP, e.getMessage());			
		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}


	@Test(priority=4, enabled=true, groups = {"regression"})
	public void SendMoneyOTC_Fail_NotEnoughBalance() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {		
			TestUtil.sleepTime(1000);
			String bal=HomeScr.Get_Balance();	
			String balance=TestUtil.Set_InsuffientBalance(bal);
			
			sm = HomeScr.goToSendMoney();			
			sm.fill_data_OTC(sm.senderPhno, sm.senderName, sm.senderNRC, sm.OTCPhno, sm.receiverName, sm.receiverNRC, balance);
			sm.click_Continue();
			sm.click_Confirm();
			String actual_result = sm.get_result();
			TestUtil.addSS2Report("SendMoneyOTC_Fail_NotEnoughBalance");
			Assert.assertEquals(actual_result,sm.err_insufBal);
			TestUtil.sleepTime(1000);
			TestUtil.click_TryAgain();	
			TestUtil.Return_HomeScreen();

		} catch(NoSuchElementException e) {
			test.log(Status.SKIP, e.getMessage());			
		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}
	
	@Test(priority=5, enabled=true, groups = {"regression"})
	public void SendMoneyOTC_SamePOI() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {		
			TestUtil.sleepTime(2000);
			sm = HomeScr.goToSendMoney();	
			// fill same POI in both receiver and sender POI
			sm.fill_data_OTC(sm.senderPhno, sm.senderName, sm.senderNRC, sm.OTCPhno, sm.receiverName, sm.senderNRC, sm.amount);
			sm.click_Continue();
			sm.click_Confirm();
			String actual_result = sm.get_result();	
			TestUtil.addSS2Report("SendMoneyMA_Fail_NonMA");
			Assert.assertEquals(actual_result, sm.err_samePOI);
			TestUtil.sleepTime(1000);	
			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();

		} catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());		

		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}

	// ------------------ Send Money to MA Failure Test Cases -----------------------------


	@Test(priority=6, enabled=true, groups = {"regression"})
	public void SendMoneyMA_Fail_SenderPartner() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {				
			TestUtil.sleepTime(1000);
			sm = HomeScr.goToSendMoney();
			sm.open_MAtab();
			sm.fill_data_MA(sm.partnerPhno,sm.senderName,sm.senderNRC,sm.MAPhno,sm.amount);
			sm.click_Continue();
			sm.click_Confirm();
//			TestUtil.sleepTime(2000);
			String actual_result = sm.get_result();	
			TestUtil.addSS2Report("SendMoneyMA_Fail_SenderPartner");
			Assert.assertEquals(actual_result, sm.err_sender);
			TestUtil.sleepTime(1000);
			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();

		} catch(NoSuchElementException e) {
			test.log(Status.SKIP, e.getMessage());			
		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}

	@Test(priority=7, enabled=true, groups = {"regression"})
	public void SendMoneyMA_Fail_ReceiverPartner() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {		
			TestUtil.sleepTime(1000);
			sm = HomeScr.goToSendMoney();
			sm.open_MAtab();
			sm.fill_data_MA(sm.senderPhno,sm.senderName,sm.senderNRC,sm.partnerPhno,sm.amount);
			sm.click_Continue();
			sm.click_Confirm();
			String actual_result = sm.get_result();
			TestUtil.addSS2Report("SendMoneyMA_Fail_ReceiverPartner");
			Assert.assertEquals(actual_result, sm.err_receiver);
			TestUtil.sleepTime(1000);
			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();

		} catch(NoSuchElementException e) {
			test.log(Status.SKIP, e.getMessage());			
		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}

	@Test(priority=8, enabled=true, groups = {"regression"})
	public void SendMoneyMA_Fail_NotEnoughBalance() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {		
			TestUtil.sleepTime(1000);
			String bal=HomeScr.Get_Balance();	
			String balance=TestUtil.Set_InsuffientBalance(bal);
			sm = HomeScr.goToSendMoney();
			sm.open_MAtab();
			sm.fill_data_MA(sm.senderPhno,sm.senderName,sm.senderNRC,sm.MAPhno,balance);
			sm.click_Continue();
			sm.click_Confirm();		
			String actual_result = sm.get_result();
			TestUtil.addSS2Report("SendMoneyMA_Fail_NotEnoughBalance");
			Assert.assertEquals(actual_result, sm.err_insufBal);	
			TestUtil.sleepTime(1000);
			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();
			
		} catch(NoSuchElementException e) {
			test.log(Status.SKIP, e.getMessage());			
		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}

	@Test(priority=9, enabled=true, groups = {"regression"})
	public void SendMoneyMA_Fail_NonMA() {	

		SendMoneyScreen sm = new SendMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {		
			TestUtil.sleepTime(1000);
			sm = HomeScr.goToSendMoney();
			sm.open_MAtab();
			sm.fill_data_MA(sm.senderPhno,sm.senderName,sm.senderNRC,sm.OTCPhno,sm.amount);
			sm.click_Continue();
			sm.click_Confirm();
			String actual_result = sm.get_result();	
			TestUtil.addSS2Report("SendMoneyMA_Fail_NonMA");
			Assert.assertEquals(actual_result, sm.err_nonWA);
			TestUtil.sleepTime(1000);
			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();

		} catch(NoSuchElementException e) {
			test.log(Status.SKIP, e.getMessage());			
		} catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}
	
	
	// Send Money OTC by using driver license
//	@Test(priority=10, enabled=true, groups = {"sanity","regression","testpartial"})
//	public void SendMoneyOTC_SuccessByDriverLicense() {
//
//		SendMoneyScreen sm = new SendMoneyScreen();
//		HomeScreen HomeScr = new HomeScreen();		
//		try {
//      
//		} catch(NoSuchElementException e) {			
//			test.log(Status.SKIP, e.getMessage());
//			TestUtil.no_element_err();
//			
//		} catch(AssertionError e) {			
//			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
//			TestUtil.Return_HomeScreen();
//		}
//	}

	
	
	// Send Money OTC by using passport
//		@Test(priority=10, enabled=true, groups = {"sanity","regression","testpartial"})
//		public void SendMoneyOTC_SuccessByPassport() {
//
//			SendMoneyScreen sm = new SendMoneyScreen();
//			HomeScreen HomeScr = new HomeScreen();		
//			try {
//	      
//			} catch(NoSuchElementException e) {			
//				test.log(Status.SKIP, e.getMessage());
//				TestUtil.no_element_err();
//				
//			} catch(AssertionError e) {			
//				test.log(Status.FAIL, e.getMessage());	
//				TestUtil.click_TryAgain();
//				TestUtil.Return_HomeScreen();
//			}
//		}
//	
	
//	@Test(priority=12, enabled=true, groups = {"sanity","regression","testpartial"})
//	public void SendMoneyOTC_Validation() {
//
//		SendMoneyScreen sm = new SendMoneyScreen();
//		HomeScreen HomeScr = new HomeScreen();
//		
//		try {        
//			
//		} catch(NoSuchElementException e) {
//			
//			test.log(Status.SKIP, e.getMessage());
//			TestUtil.no_element_err();
//			
//		} catch(AssertionError e) {
//			
//			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
//			TestUtil.Return_HomeScreen();
//		}
//	}

	

	@AfterClass
	public void tearDownSuite() {
		driver.quit();	
	}

}
