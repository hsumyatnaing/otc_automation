package com.wave.channel.testcases;

import java.lang.reflect.Method;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.wave.channel.util.TestUtil;
import com.wave.channel.base.TestBase;
import com.wave.channel.pages.Billers;
import com.wave.channel.pages.HomeScreen;
import com.wave.channel.pages.PaymentScreen;
import com.wave.channel.util.BillerJsonUtil;

public class BillersTest extends TestBase implements ITest {

	@BeforeClass(alwaysRun=false)
	public void setUp()  {		
		initialize();	
		TestUtil.loginApp();	
	}	
	//////////////////////////////////////////////////////////////////

	// Code to add dynamic name in Report 
	private ThreadLocal<String> testName = new ThreadLocal<>();

	@BeforeMethod(groups="regression")
	public void BeforeMethod(Method method, Object[] testData){
		testName.set(method.getName() + "_" + testData[0]);
	}

	@Override
	public String getTestName() {
		return testName.get();
	}
	////////////////////////////////////////////////////////////////////

	JSONObject billersJson;

	@DataProvider(name = "billersData")
	public Object[][] getData() {

		Object[][] arr = null;
		BillerJsonUtil util = new BillerJsonUtil();
		arr = util.getBillerTestData();
		return arr;
	}


	String currentBillerAmount = "";
	/*
	 * Create test data for all billers
	 * */
	@Test(dataProvider = "billersData",groups= {"sanity","regression"})	
	public void TestBillers(Object billerNameKey, Object data, Object fee, Object comm) {

		HomeScreen HomeScr = new HomeScreen();
		Billers billers = new Billers();

		String billerName = (String) billerNameKey;
		String feeAmt = (String) fee;
		String commAmt = (String) comm;		

		JSONObject biller = (JSONObject) data;
		JSONArray fieldDetails = (JSONArray) biller.get("fieldsDetails");				

		try {

			TestUtil.sleepTime(1000);

			// get current wallet balance
			String bef_bal=HomeScr.Get_Balance();		

			// go to payment screen
			PaymentScreen PaymentScr = HomeScr.gotoPayment();			

			// wait while billers are loading
			TestUtil.sleepTime(1000);             
			TestUtil.visibilityEleWaitByLocator(By.className("androidx.recyclerview.widget.RecyclerView"));            

			// find Biller 
			TestUtil.scroll2Element(billerName); 
	      
			// open Biller
			billers.open_Biller(billerName);

			// enter data in input fields
			billers.fillInput(fieldDetails);              

			// click Continue button to navigate to Summary page
			billers.click_Continue(); 

			// click Confirm button to make payment and get summary view items and tid
			billers.click_Confirm();

			// take screenshot of Successful screen and add to report
			TestUtil.addSS2Report(billerName);           

			TestUtil.sleepTime(1000); 

			// assert if payment is successful or not
			String actual_result = billers.get_result();
			Assert.assertEquals(actual_result, billers.expectedResult_Success);     
			logger.info("Actual result \"" + actual_result + "\" is same as the expected result \"" + billers.expectedResult_Success + "\" " );

			// scroll down to Transaction ID 
			String ele = "Transaction ID";
			TestUtil.scroll2Element(ele);

			// click 'I have taken the money' button' to navigate to Home screen
			TestUtil.sleepTime(1000);
			TestUtil.Click_Success();			

			// get current wallet balance
			TestUtil.sleepTime(2000);
			String aft_bal=HomeScr.Get_Balance();	

			// calculate balance deduction 
			billers.check_BalanceDeduction(bef_bal, aft_bal,feeAmt,commAmt);


		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception >>> " + e);
			test.fail("Exception >>> " + e);
			TestUtil.no_element_err();
		}

	}

	@AfterClass(alwaysRun=false)
	public void tearDown() {
		driver.quit();
	}


}
