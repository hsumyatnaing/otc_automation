package com.wave.channel.testcases;

import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.pages.EtopupScreen;
import com.wave.channel.pages.HomeScreen;
import com.wave.channel.util.TestUtil;

public class TopupTest extends TestBase {

	@BeforeClass
	public void setupClass() {		
		initialize();
		 TestUtil.loginApp();
	}

	@Test(priority=0, enabled=true, groups= {"regression"})
	public void Telenor_SuccessTopup() {	
		
		EtopupScreen page = new EtopupScreen();	
		HomeScreen HomeScr = new HomeScreen();
		
		try {					
			TestUtil.sleepTime(1000); // to remove
			// get wallet balance
			String bef_bal=HomeScr.Get_Balance();	
            // open Topup screen
			page = HomeScr.gotoEtopup();
			TestUtil.sleepTime(1000);
            // select Telenor operator
			page.select_Operator(1);
			TestUtil.sleepTime(1000);
            // fill telenor MSISDN
			page.fill_Number(page.telenor);
            // choose amount
			page.choose_TopupAmount();
			TestUtil.sleepTime(1000);			
            // click Continue button
			page.click_Continue();
            // click Confirm button
			page.click_Confrim();
            // add screenshot to report 
			TestUtil.addSS2Report("Telenor_SuccessTopup");
			String actual_result = page.get_result();
			Assert.assertEquals(actual_result, "Successful");
			TestUtil.get_TID(page.txt_Summary,page.index);				
			page.click_Finish();
			TestUtil.sleepTime(2000);
			String aft_bal=HomeScr.Get_Balance();	
			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, page.amt,page.fee ,page.telnorcom);	// check balance deduction
		}
		catch(NoSuchElementException e) {			
			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();			
		}catch(AssertionError e) {			
			test.log(Status.FAIL, e.getMessage());
			page.click_TryAgain();
			page.backto_Home();			
		}
	}

	@Test(priority=1, enabled=true, groups= {"regression"})
	public void MPT_SuccessTopup() {	

		EtopupScreen page = new EtopupScreen();	
		HomeScreen HomeScr = new HomeScreen();

		try {

			TestUtil.sleepTime(1000); // to remove
            // get wallet balance
			String bef_bal=HomeScr.Get_Balance();	
            // open Topup screen
			page = HomeScr.gotoEtopup();
			TestUtil.sleepTime(1000);
            // select MPT operator
			page.select_Operator(3);
			TestUtil.sleepTime(1000);
            // fill MPT MSISDN
			page.fill_Number(page.mpt);
            // choose Topup amount
			page.choose_TopupAmount();
			TestUtil.sleepTime(1000);			
            // click Continue button
			page.click_Continue();
            // click Confirm button
			page.click_Confrim();

			TestUtil.sleepTime(3000);
            
			TestUtil.addSS2Report("MPT_SuccessTopup");

			String actual_result = page.get_result();

			Assert.assertEquals(actual_result, "Successful");
			TestUtil.get_TID(page.txt_Summary,page.index);	
			page.click_Finish();

			TestUtil.sleepTime(2000);
			String aft_bal=HomeScr.Get_Balance();	

			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, page.amt,page.fee ,page.mptCom);	// check balance deduction	

		}
		catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();

		}catch(AssertionError e) {

			test.log(Status.FAIL, e.getMessage());
			page.click_TryAgain();
			page.backto_Home();
		}
	}


	@Test(priority=2, enabled=true, groups= {"regression"})
	public void Ooredoo_SuccessTopup() {	
		EtopupScreen page = new EtopupScreen();	
		try {

			HomeScreen HomeScr = new HomeScreen();
			TestUtil.sleepTime(1000); // to remove

			String bef_bal=HomeScr.Get_Balance();	

			page = HomeScr.gotoEtopup();
			TestUtil.sleepTime(1000);

			page.select_Operator(2);
			TestUtil.sleepTime(1000);

			page.fill_Number(page.ooredoo);

			page.choose_TopupAmount();
			TestUtil.sleepTime(1000);			

			page.click_Continue();

			page.click_Confrim();

			TestUtil.sleepTime(3000);

			TestUtil.addSS2Report("Ooredoo_SuccessTopup");

			String actual_result = page.get_result();
			Assert.assertEquals(actual_result, "Successful");
			TestUtil.get_TID(page.txt_Summary,page.index);	
			page.click_Finish();

			TestUtil.sleepTime(2000);
			String aft_bal=HomeScr.Get_Balance();	

			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, page.amt,page.fee ,page.ooredooCom);	// check balance deduction	

		}
		catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();

		}catch(AssertionError e) {

			test.log(Status.FAIL, e.getMessage());
			page.click_TryAgain();
			page.backto_Home();


		}
	}
	
	// Removed Operator

//	@Test(priority=3, enabled=true)
//	public void OMO_SuccessTopup() {	
//		
//		EtopupScreen page = new EtopupScreen();	
//		HomeScreen HomeScr = new HomeScreen();
//		
//		try {			
//			TestUtil.sleepTime(1000); // to remove
//
//			String bef_bal=HomeScr.Get_Balance();	
//
//			page = HomeScr.gotoEtopup();
//			TestUtil.sleepTime(1000);
//
//			page.select_Operator(4);
//			TestUtil.sleepTime(1000);
//
//			page.fill_Number(page.mytel);
//
//			page.choose_Amount();
//			TestUtil.sleepTime(1000);			
//
//			page.click_Continue();
//
//			page.click_Confrim();
//
//			TestUtil.sleepTime(3000);
//
//			TestUtil.addSS2Report("MyTel_SuccessTopup");
//
//			String actual_result = page.get_result();
//			Assert.assertEquals(actual_result, "Successful");
//			TestUtil.get_TID(page.txt_Summary,page.index);	
//			page.click_Finish();
//
//			TestUtil.sleepTime(2000);
//			String aft_bal=HomeScr.Get_Balance();	
//
//			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, page.amt,page.fee ,page.mytelCom);	// check balance deduction	
//
//		}
//		catch(NoSuchElementException e) {
//
//			test.log(Status.SKIP, e.getMessage());
//		}
//	}
	
	@Test(priority=0, enabled=true, groups= {"regression"})
	public void Telenor_SuccessDataPack() {	
		
		EtopupScreen page = new EtopupScreen();	
		HomeScreen HomeScr = new HomeScreen();
		
		try {					
			TestUtil.sleepTime(1000); 		
			String bef_bal=HomeScr.Get_Balance();	          
			page = HomeScr.gotoEtopup();
			TestUtil.sleepTime(1000);  
			page.open_DataPacksTab();
			page.select_Operator(1);
			TestUtil.sleepTime(1000);           
			page.fill_Number(page.telenor);           
			page.choose_DataPack();
			TestUtil.sleepTime(1000);	          
			page.click_DataPackContinue();           
			page.click_Confrim();           
			TestUtil.addSS2Report("Telenor_SuccessDataPack");
			String actual_result = page.get_result();
			Assert.assertEquals(actual_result, "Successful");
			TestUtil.get_TID(page.txt_Summary,page.index);				
			page.click_Finish();
			TestUtil.sleepTime(2000);
			String aft_bal=HomeScr.Get_Balance();	
			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, page.telenorPackamt,page.fee ,page.telenorPackCom);	// check balance deduction
		}
		catch(NoSuchElementException e) {			
			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();			
		}catch(AssertionError e) {			
			test.log(Status.FAIL, e.getMessage());
			page.click_TryAgain();
			page.backto_Home();			
		}
	}
	

	@Test(priority=1, enabled=true, groups= {"regression"})
	public void MPT_SuccessDataPack() {	

		EtopupScreen page = new EtopupScreen();	
		HomeScreen HomeScr = new HomeScreen();

		try {
			TestUtil.sleepTime(1000); // to remove           
			String bef_bal=HomeScr.Get_Balance();	           
			page = HomeScr.gotoEtopup();
			TestUtil.sleepTime(1000);
			page.open_DataPacksTab();        
			page.select_Operator(3);
			TestUtil.sleepTime(1000);           
			page.fill_Number(page.mpt);       
			page.choose_DataPack();
			TestUtil.sleepTime(1000);          
            page.click_DataPackContinue();          
			page.click_Confrim();
			TestUtil.sleepTime(3000);            
			TestUtil.addSS2Report("MPT_SuccessDataPack");
			String actual_result = page.get_result();
			Assert.assertEquals(actual_result, "Successful");
			TestUtil.get_TID(page.txt_Summary,page.datapackIndex);	
			page.click_Finish();
			TestUtil.sleepTime(2000);
			String aft_bal=HomeScr.Get_Balance();
			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, page.mptPackamt,page.fee ,page.mptPackCom);	// check balance deduction	

		}
		catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();

		}catch(AssertionError e) {

			test.log(Status.FAIL, e.getMessage());
			page.click_TryAgain();
			page.backto_Home();
		}
	}
	
	@AfterClass
	public void tearDownSuite() {
		driver.quit();	
	}

}
