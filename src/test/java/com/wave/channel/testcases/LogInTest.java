package com.wave.channel.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.pages.HomeScreen;
import com.wave.channel.pages.LaunchScreen;
import com.wave.channel.pages.PinScreen;
import com.wave.channel.util.TestUtil;


public class LogInTest extends TestBase{	

	@BeforeClass(alwaysRun=false)
	public void setUp()  {		
		initialize();			
	}

	@BeforeGroups("testpartial")
	public void setUp_2()  {		
		initialize();			
	}	

	@BeforeGroups("regression")
	public void setUp_3()  {		
		initialize();			 
	}


	@Test(enabled=true,priority=1,groups= {"sanity","regression","testpartial"})
	public void Login_Success() {	
		LaunchScreen launchScr = new LaunchScreen();
		PinScreen pinScr = new PinScreen();
		HomeScreen homeScr = new HomeScreen();
		try {		
			launchScr.changeENG();			

			launchScr.enterMSISDN(prop.getProperty("misdn"));		

			pinScr = launchScr.gotoPINScreen();			

			TestUtil.sleepTime(1000);
			pinScr.enterPIN(prop.getProperty("pin"));			

			TestUtil.sleepTime(2000); // need to check
			homeScr = pinScr.clickLogInBtn();			

			Assert.assertEquals(homeScr.getTitle(), homeScr.expectedResult_Success);

		}catch(AssertionError e) {
			test.log(Status.FAIL, e.getMessage());
		}
	}


	//	@Test(enabled=true,priority=1,groups= {"sanity","regression"})
	//	public void Login_Fail() {	
	//		LaunchScreen launchScr = new LaunchScreen();
	//		PinScreen pinScr = new PinScreen();
	//		
	//		try {		
	//			launchScr.changeENG();
	//			launchScr.enterMSISDN(prop.getProperty("misdn"));
	//			pinScr = launchScr.gotoPINScreen();
	//			TestUtil.sleepTime(1000);
	//			pinScr.enterPIN("1234");
	//			TestUtil.sleepTime(3000);
	//			pinScr.clickLogInBtn();	
	//			Assert.assertEquals(pinScr.getPinError(), pinScr.pinError);
	//			pinScr.enterPIN("1234");
	//
	//		}catch(AssertionError e) {
	//			test.log(Status.FAIL, e.getMessage());
	//		}
	//	}


	@AfterClass(alwaysRun=false)
	public void tearDown() {
		driver.quit();
	}

	@AfterGroups("testpartial")
	public void tearDown_2() {
		driver.quit();
	}

	@AfterGroups("regression")
	public void tearDown_3() {
		driver.quit();
	}
}
