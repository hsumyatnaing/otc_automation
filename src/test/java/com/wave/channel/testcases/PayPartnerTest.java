package com.wave.channel.testcases;

import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.pages.HomeScreen;
import com.wave.channel.pages.PartnerScreen;
import com.wave.channel.util.TestUtil;

public class PayPartnerTest extends TestBase {
	
	@BeforeClass(alwaysRun=false)
	public void setupClass() {		
		initialize();
		TestUtil.loginApp();
	}
	
	@Test(groups= {"sanity","regression"},priority=0)
	public void PayPartner_SuccessPayment() {	
		
		PartnerScreen testScreen = new PartnerScreen();
		HomeScreen HomeScr = new HomeScreen();
		
		try {	
		TestUtil.sleepTime(1000); 
		String bef_bal=HomeScr.Get_Balance();	
		testScreen = HomeScr.openParnterTab();
		testScreen.open_PayPartner();
		testScreen.do_Payment(testScreen.CSE, testScreen.amt);
		TestUtil.sleepTime(1000);
		String actual_result = testScreen.get_result();
		TestUtil.sleepTime(1000);
		TestUtil.addSS2Report("PayPartner_SuccessPayment");	
		Assert.assertEquals(actual_result, TestUtil.expectedResult_Success);	
		TestUtil.get_TID(testScreen.txt_Summary,testScreen.index);
		TestUtil.Click_Success();		
		TestUtil.sleepTime(1000);
		String aft_bal=HomeScr.Get_Balance();		
		TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, testScreen.amt,testScreen.fee ,testScreen.com );
			
		} catch(NoSuchElementException e) {			
			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();
			
		}catch(AssertionError e) {			
			test.log(Status.FAIL, e.getMessage());
			TestUtil.no_element_err();		
		}
	}
	
	
	@Test(groups= {"sanity","regression"},priority=1)
	public void PayPartner_Fail() {	
		
		PartnerScreen testScreen = new PartnerScreen();
		HomeScreen HomeScr = new HomeScreen();
		
		try {	
		TestUtil.sleepTime(1000); 	
		testScreen = HomeScr.openParnterTab();
		testScreen.open_PayPartner();
		testScreen.do_Payment(testScreen.FoFo, testScreen.amt);
		TestUtil.sleepTime(1000);
		String actual_result = testScreen.get_result();
		TestUtil.sleepTime(1000);
		TestUtil.addSS2Report("PayPartner_Fail");	
		Assert.assertEquals(actual_result, testScreen.expectedResult_Fail);		
        TestUtil.click_TryAgain();
        TestUtil.back2Home();
        TestUtil.Click_HomeMenu();		
	
		} catch(NoSuchElementException e) {
			
			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();
			
		}catch(AssertionError e) {
			
			test.log(Status.FAIL, e.getMessage());
			TestUtil.no_element_err();
		
		}
	}
	
	
	@AfterClass
	public void tearDownSuite() {
		driver.quit();	
	}

}
