package com.wave.channel.testcases;

import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.pages.HomeScreen;
import com.wave.channel.pages.ReceiveMoneyScreen;
import com.wave.channel.util.TestUtil;

public class ReceiveMoneyTest extends TestBase {

	@BeforeClass
	public void setupClass() {		
		initialize();
		TestUtil.loginApp();
	}

	// before run the Receive Money (MA-OTC) ,
	// it is need to update TID in Data.json file since one tid can be received money just one time

	// balance should be between 1~10,000 , if not fee and commission need to be updated in Data.json file too
	// since fee and commission are different for each slab

	// Pin should be set as 123456 , if not you can change it in Data.json

	@Test(enabled=true,priority = 2)
	public void ReceiveMoneyMA2OTC_Success() {	

		ReceiveMoneyScreen rm = new ReceiveMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {

			TestUtil.sleepTime(1000); // to remove
			String bef_bal=HomeScr.Get_Balance();	
			rm = HomeScr.goToReceiveMoney();
			rm.fill_data(rm.tid,rm.pin);
			rm.click_Continue();
			rm.click_Confirm();
			TestUtil.sleepTime(1000);

			TestUtil.sleepTime(1000);
			String actual_result = rm.get_result();

			TestUtil.sleepTime(1000);
			TestUtil.addSS2Report("ReceiveMoney_Success");	
			Assert.assertEquals(actual_result, "Successful");	

			TestUtil.get_TID(rm.txt_Summary,rm.index);

			TestUtil.Click_Success();

			TestUtil.sleepTime(2000);

			String aft_bal=HomeScr.Get_Balance();

			TestUtil.Check_RM_Calcuation(bef_bal, aft_bal, rm.amt,rm.fee ,rm.com);



		} catch(NoSuchElementException e) {

			test.log(Status.INFO, e.getMessage());

		}
	}

	@Test(priority = 0)
	public void ReceiveMoneyMA2OTC_Fail_InvalidTID() {	
		ReceiveMoneyScreen rm = new ReceiveMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();
		try {	   
			TestUtil.sleepTime(1000); // to remove

			rm = HomeScr.goToReceiveMoney();
			rm.fill_data(rm.invalidtid,rm.pin);
			rm.click_Continue();
			rm.click_Confirm();
			TestUtil.sleepTime(1000);		

			String actual_result = rm.get_result();
			TestUtil.sleepTime(1000);	
			TestUtil.addSS2Report("ReceiveMoneyMA2OTC_Fail_InvalidTID");	
			Assert.assertEquals(actual_result, rm.err_invalidTID );	

			TestUtil.click_TryAgain();
			TestUtil.back2Home();


		} catch(NoSuchElementException e) {

			test.log(Status.INFO, e.getMessage());

		}
	}

	@Test(priority=1)
	public void ReceiveMoneyMA2OTC_Fail_InvalidPIN() {	
		ReceiveMoneyScreen rm = new ReceiveMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();
		try {	   
			TestUtil.sleepTime(1000); // to remove
			rm = HomeScr.goToReceiveMoney();
			rm.fill_data(rm.tid,rm.invalidpin);
			rm.click_Continue();
			rm.click_Confirm();
			TestUtil.sleepTime(1000);		

			String actual_result = rm.get_result();
			TestUtil.sleepTime(1000);	
			TestUtil.addSS2Report("ReceiveMoneyMA2OTC_Fail_InvalidPIN");	
			Assert.assertEquals(actual_result, rm.err_invalidPIN);	

			TestUtil.click_TryAgain();
			TestUtil.back2Home();

		} catch(NoSuchElementException e) {

			test.log(Status.INFO, e.getMessage());

		}
	}

	// need to update 
	@Test(enabled=true,priority = 3,groups= {"sanity","regression","testpartial"})
	public void ReceiveMoneyOTC2OTC_Success() {	

		ReceiveMoneyScreen rm = new ReceiveMoneyScreen();
		HomeScreen HomeScr = new HomeScreen();

		try {

			TestUtil.sleepTime(1000); // to remove
			String bef_bal=HomeScr.Get_Balance();	
			rm = HomeScr.goToReceiveMoney();
			rm.fill_data(tid_SendMoney2OTC,secretCode_OTC); 
			System.out.println("OTC : " + tid_SendMoney2OTC + "SC : " + secretCode_OTC );
			rm.click_Continue();
			rm.click_Confirm();
			TestUtil.sleepTime(1000);

			TestUtil.sleepTime(1000);
			String actual_result = rm.get_result();

			TestUtil.sleepTime(1000);
			TestUtil.addSS2Report("ReceiveMoney_Success");	
			Assert.assertEquals(actual_result, "Successful");
			TestUtil.get_TID(rm.txt_Summary,rm.index);
			TestUtil.Click_Success();

			TestUtil.sleepTime(2000);

			String aft_bal=HomeScr.Get_Balance();

			TestUtil.Check_RM_Calcuation(bef_bal, aft_bal, rm.amt,rm.fee ,rm.com);	

		} catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());
			TestUtil.no_element_err();

		}catch(AssertionError e) {

			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
			TestUtil.no_element_err();
		}
	}


	@AfterClass
	public void tearDownSuite() {
		driver.quit();	
	}

}
