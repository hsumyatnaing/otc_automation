package com.wave.channel.testcases;

import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.pages.CashInScreen;
import com.wave.channel.pages.HomeScreen;
import com.wave.channel.util.TestUtil;

public class CashInTest extends TestBase {

	@BeforeClass(alwaysRun=false)
	public void setupClass() {		
		initialize();
		TestUtil.loginApp();
	}

	@Test (priority = 0, enabled = true, groups = {"sanity","regression"})
	public void CashIn_Success() {	
		
		CashInScreen ci = new CashInScreen();		
		HomeScreen HomeScr = new HomeScreen();		
		try {			
			TestUtil.sleepTime(1000); 
			String bef_bal=HomeScr.Get_Balance();	
			ci = HomeScr.gotoCashIn();		
			ci.fill_Data(ci.l2phNo,ci.amt);	
			TestUtil.sleepTime(2000);
			TestUtil.addSS2Report("CashIn_Success");
			String actual_result = ci.get_result();
			Assert.assertEquals(actual_result, ci.expectedResult_Success);	
			TestUtil.get_TID(ci.txt_Summary,ci.index);
			TestUtil.Click_Success();
			TestUtil.sleepTime(2000);
			String aft_bal=HomeScr.Get_Balance();
			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, ci.amt,ci.fee ,ci.com );

		}catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());	
			TestUtil.no_element_err();
			
		} catch(AssertionError e) {	
			
			test.log(Status.FAIL, e.getMessage());
//			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();
			
		}
	}


	@Test(priority = 1, enabled = true, groups = {"regression"})
	public void CashIn_Fail_NonMA() {	
		CashInScreen ci = new CashInScreen();		
		HomeScreen HomeScr = new HomeScreen();
		try {

			TestUtil.sleepTime(1000); 
			ci = HomeScr.gotoCashIn();			
			TestUtil.sleepTime(2000); 
			ci.fill_Data(ci.otcNo,ci.amt);	
			TestUtil.sleepTime(2000);
			TestUtil.addSS2Report("CashIn_Fail_NonMA");
			String actual_result = ci.get_result();
			Assert.assertEquals(actual_result, ci.err_nonWA);			
			TestUtil.click_TryAgain();
			TestUtil.back2Home();
		
		}catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());	
			TestUtil.no_element_err();
			
		} catch(AssertionError e) {	
			
			test.log(Status.FAIL, e.getMessage());
//			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();
			
		}
	}

	@Test(priority = 2, enabled=true, groups = {"regression"})
	public void CashIn_Fail_ExceedBalance() {	
		CashInScreen ci = new CashInScreen();		
		HomeScreen HomeScr = new HomeScreen();
		try {

			TestUtil.sleepTime(1000); 	
			ci = HomeScr.gotoCashIn();		
			ci.fill_Data(ci.l1phNo,ci.exceedamt);	
			TestUtil.sleepTime(2000);
			TestUtil.addSS2Report("CashIn_Fail_ExceedBalance");
			String actual_result = ci.get_result();
			Assert.assertEquals(actual_result, ci.err_walletExceeds);			
			TestUtil.click_TryAgain();
			TestUtil.back2Home();
						

		}catch(NoSuchElementException e) {

			test.log(Status.SKIP, e.getMessage());	
			TestUtil.no_element_err();
			
		} catch(AssertionError e) {	
			
			test.log(Status.FAIL, e.getMessage());
//			TestUtil.click_TryAgain();
			TestUtil.Return_HomeScreen();
			
		}
	}

	@AfterClass
	public void tearDownSuite() {
		driver.quit();	
	}
}
