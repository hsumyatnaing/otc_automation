//package com.wave.channel.testcases;
//
//import org.openqa.selenium.NoSuchElementException;
//import org.testng.Assert;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//import com.aventstack.extentreports.Status;
//import com.wave.channel.base.TestBase;
//import com.wave.channel.pages.HomeScreen;
//import com.wave.channel.pages.SendMoneyScreen;
//import com.wave.channel.util.TestUtil;
//
//public class testsendmoney extends TestBase {
//	
//	
//	@BeforeClass(alwaysRun=false)
//	public void setupClass() {		
//		initialize();	
//		TestUtil.loginApp();
//	}
//
//	@Test(priority=0, enabled=true, groups = {"sanity","regression"})
//	public void SendMoneyOTC_Success() {
//
//		SendMoneyScreen sm = new SendMoneyScreen();
//		HomeScreen HomeScr = new HomeScreen();
//		
//		try {
//            TestUtil.sleepTime(1000);            
//			String bef_bal=HomeScr.Get_Balance();
//			sm = HomeScr.goToSendMoney(); 		
//			sm.fill_data_OTC_2(sm.senderPhno, sm.senderName, sm.senderNRC, sm.OTCPhno, sm.receiverName, sm.receiverNRC, sm.amount); // fill data in each field
//			sm.click_Continue(); // click Continue button
//			sm.click_Confirm(); // click Confirm button
//			TestUtil.visibilityEleWaitByLocator(sm.btnFinish);	// waiting time to see success screen
//			String actual_result = sm.get_result();	// get result
//			TestUtil.addSS2Report("SendMoneyOTC_Success");	// take SS and add to report
//			Assert.assertEquals(actual_result, sm.expectedResult_Success);	// compare the expected result and actual result
//			TestUtil.scroll2Element("Date"); // scroll down			
//			tid_SendMoney2OTC = TestUtil.get_TID(sm.txt_Summary,sm.indexOTC);  // keep TID for receive Money script            			
//			TestUtil.Click_BillSuccess(); // click button to go back to Home page
//			String aft_bal=HomeScr.Get_Balance(); // get after wallet balance
//			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, sm.amount ,sm.fee ,sm.com); // balance deduction calculation
//		
//		} catch(NoSuchElementException e) {
//			
//			test.log(Status.SKIP, e.getMessage());
//			TestUtil.no_element_err();
//			
//		} catch(AssertionError e) {
//			
//			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
//			TestUtil.Return_HomeScreen();
//		}
//	}
//	
//	
//	@Test(priority=0, enabled=true, groups = {"sanity","regression"})
//	public void SendMoneyOTC_Validation() {
//
//		SendMoneyScreen sm = new SendMoneyScreen();
//		HomeScreen HomeScr = new HomeScreen();
//		
//		try {
//            TestUtil.sleepTime(1000);            
//			String bef_bal=HomeScr.Get_Balance();
//			sm = HomeScr.goToSendMoney(); 		
//			sm.fill_data_OTC_2(sm.senderPhno, sm.senderName, sm.senderNRC, sm.OTCPhno, sm.receiverName, sm.receiverNRC, sm.amount); // fill data in each field
//			sm.click_Continue(); // click Continue button
//			sm.click_Confirm(); // click Confirm button
//			TestUtil.visibilityEleWaitByLocator(sm.btnFinish);	// waiting time to see success screen
//			String actual_result = sm.get_result();	// get result
//			TestUtil.addSS2Report("SendMoneyOTC_Success");	// take SS and add to report
//			Assert.assertEquals(actual_result, sm.expectedResult_Success);	// compare the expected result and actual result
//			TestUtil.scroll2Element("Date"); // scroll down			
//			tid_SendMoney2OTC = TestUtil.get_TID(sm.txt_Summary,sm.indexOTC);  // keep TID for receive Money script            			
//			TestUtil.Click_BillSuccess(); // click button to go back to Home page
//			String aft_bal=HomeScr.Get_Balance(); // get after wallet balance
//			TestUtil.Check_BalanceDeduction(bef_bal, aft_bal, sm.amount ,sm.fee ,sm.com); // balance deduction calculation
//			
//		} catch(NoSuchElementException e) {
//			
//			test.log(Status.SKIP, e.getMessage());
//			TestUtil.no_element_err();
//			
//		} catch(AssertionError e) {
//			
//			test.log(Status.FAIL, e.getMessage());	
//			TestUtil.click_TryAgain();
//			TestUtil.Return_HomeScreen();
//		}
//	}
//
//	@AfterClass
//	public void tearDownSuite() {
//		driver.quit();	
//	}
//
//}
