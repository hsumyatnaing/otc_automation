package com.wave.channel.pages;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.TestUtil;

public class PinScreen extends TestBase {
	
	public PinScreen() {		
		PageFactory.initElements(driver, this);
	}
	
	public String pinError = "Incorrect PIN. Please try again.";
	
	@FindBy(id = "dialpad0")
	public WebElement dial_0;

	@FindBy(id = "dialpad1")
	public WebElement dial_1;

	@FindBy(id = "dialpad2")
	public WebElement dial_2;

	@FindBy(id = "dialpad3")
	public WebElement dial_3;

	@FindBy(id = "dialpad4")
	public WebElement dial_4;

	@FindBy(id = "dialpad5")
	public WebElement dial_5;

	@FindBy(id = "dialpad6")
	public WebElement dial_6;

	@FindBy(id = "dialpad7")
	public WebElement dial_7;

	@FindBy(id = "dialpad8")
	public WebElement dial_8;

	@FindBy(id = "dialpad9")
	public WebElement dial_9;	

	@FindBy(id= "btn_login")
	public WebElement btn_login;

	@FindBy(id= "btn_forgotpin")
	public WebElement btn_forgotpin;

	@FindBy (id="language_logo")
	public WebElement btn_lang;
	
	@FindBy(id="language_text") 
	public List<WebElement> lst_language;
	
	@FindBy(id="txt_error") 
	public WebElement txt_incorrectpin;
	
	public void enterPIN(String PIN) {
		
		logger.info("Enter PIN");
		test.log(Status.INFO, "Enter PIN");
        
		char ch = 'i';
		for(int i=0; i<PIN.length();i++){
			ch  = PIN.charAt(i);	
		
			switch(ch) {
			case '1' :
				dial_1.click();					
				break;
			case '2' :
				dial_2.click();			
				break;
			case '3' :
				dial_3.click();				
				break;
			case '4' :
				dial_4.click();				
				break;
			case '5' :
				dial_5.click();				
				break;
			case '6' :
				dial_6.click();				
			case '7' :
				dial_7.click();				
				break;
			case '8' :
				dial_8.click();				
				break;
			case '9' :
				dial_9.click();				
				break;
			case '0' :
				dial_0.click();					
				break;
			}
		}
		TestUtil.sleepTime(1000);
	}

	public HomeScreen clickLogInBtn() {	
		logger.info("Log In");
		test.log(Status.INFO, "Click Log In Button");
		
		btn_login.click(); 
		TestUtil.sleepTime(1500);		
		TestUtil.visibilityEleWaitByLocator(By.id("balance_text"));
		return new HomeScreen();
	}
	
	public String getPinError() {
		return txt_incorrectpin.getText();
	}
}
