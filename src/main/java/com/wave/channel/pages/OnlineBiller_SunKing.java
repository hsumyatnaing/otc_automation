package com.wave.channel.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.jayway.jsonpath.JsonPath;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.DataUtil;
import com.wave.channel.util.TestUtil;

public class OnlineBiller_SunKing extends TestBase{
	
	public OnlineBiller_SunKing() {
		PageFactory.initElements(driver, this);
	}
	
	public int label= 19;
	public int index= 20;
	
	String Biller_Data = DataUtil.get_Data();	
	public String accNo = (String)JsonPath.read(Biller_Data,"$.BillerData.SunKing.accNo");
	public String phNo = (String)JsonPath.read(Biller_Data,"$.BillerData.SunKing.phNo");
	public String amt = (String)JsonPath.read(Biller_Data,"$.BillerData.SunKing.amt");
    public String fee =(String)JsonPath.read(Biller_Data,"$.BillerData.SunKing.fee"); 
	public String commision = (String)JsonPath.read(Biller_Data,"$.BillerData.SunKing.bonus");
	public String err_phno = (String)JsonPath.read(Biller_Data,"$.BillerData.SunKing.err_phno");	
	public String err_amt = (String)JsonPath.read(Biller_Data,"$.BillerData.SunKing.err_amt");	
	public String err_required = (String)JsonPath.read(Biller_Data,"$.invalidData.err_required");
	
	public String invalid_phNo = (String)JsonPath.read(Biller_Data,"$.invalidData.phNo");
	public String invalid_amt = (String)JsonPath.read(Biller_Data,"$.invalidData.amt2");
	
	
	@FindBy(className ="android.widget.EditText") 
	public List<WebElement> Txt_field;

	@FindBy(id = "btn_biller_continue")
	public WebElement btn_Continue;	

	@FindBy(id="btn_initial_continue")
	public WebElement btn_Confrim;	
	
	@FindBy(id="textinput_error")
	public WebElement txt_error;	

	@FindBy(className ="android.widget.TextView")
	public List<WebElement> txt_Summary;	
	
	@FindBy(className ="android.widget.TextView")
	public List<WebElement> txt_Err;	
	
	public void fill_ValidData(String accNo,String phNo, String amt) 
	{		
		TestUtil.sleepTime(1000);
		Txt_field.get(0).sendKeys(accNo);
		Txt_field.get(1).sendKeys(phNo);
		Txt_field.get(2).sendKeys(amt);
		driver.hideKeyboard();
		TestUtil.sleepTime(1000);
	}
	
	public void click_field(int index) {
		Txt_field.get(index).click();
		TestUtil.sleepTime(1000);
	}

	
	public String get_validationError(int index) {
		String err = txt_Err.get(index).getText();
		return err;
	}

}
