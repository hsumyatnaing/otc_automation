package com.wave.channel.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.jayway.jsonpath.JsonPath;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.DataUtil;
import com.wave.channel.util.TestUtil;

public class CashInScreen extends TestBase {

	public CashInScreen() 
	{	
		PageFactory.initElements(driver, this);
	}
	public String expectedResult_Success = "Successful";
	
	public int label= 9;
	public int index= 10;
		
	String Test_Data = DataUtil.get_Data();
	public String l2phNo = (String)JsonPath.read(Test_Data,"$.CashIn.level2Verified");
	public String l1phNo = (String)JsonPath.read(Test_Data,"$.CashIn.level1");
	public String amt = (String)JsonPath.read(Test_Data,"$.CashIn.amt");
	public String exceedamt = (String)JsonPath.read(Test_Data,"$.CashIn.exceedamt");
	public String com = (String)JsonPath.read(Test_Data,"$.CashIn.com");
	public String fee = (String)JsonPath.read(Test_Data,"$.CashIn.fee");
	public String otcNo =  (String)JsonPath.read(Test_Data,"$.CashIn.otcNo");
	
	String Error_Msg = DataUtil.get_Error();
	public String err_nonWA = (String)JsonPath.read(Error_Msg,"$.CashIn.err_nonWA");	
	public String err_walletExceeds = (String)JsonPath.read(Error_Msg,"$.CashIn.err_walletExceeds");
	
		
	@FindBy(className ="android.widget.EditText") 
	public List<WebElement> Txt_field;
	
	@FindBy(id = "btn_continue")
	public WebElement btn_Continue;	
	
	@FindBy(id = "btn_initial_continue")
	public WebElement btn_Confirm;
		
	@FindBy(className ="android.widget.TextView")
	public List<WebElement> txt_Summary;
	
	public void click_Continue() {
		btn_Continue.click();
		TestUtil.sleepTime(1000);
	}
	
	public void click_Confirm() {
		btn_Confirm.click();
		TestUtil.sleepTime(1000);
	}
	
	public String get_result() {
		String result =txt_Summary.get(0).getText();
		return result;
	}
	
	public void get_TID() {		
		String lable=txt_Summary.get(9).getText(); 
		String tid=txt_Summary.get(10).getText();
		System.out.println(lable + " " + tid);
	}
	
	public void fill_Data(String phNum,String amount) 
	{		
		TestUtil.sleepTime(1000);
		Txt_field.get(0).sendKeys(phNum);
		Txt_field.get(1).sendKeys(amount);	
		driver.hideKeyboard();
		click_Continue();
		click_Confirm();		
		
	}
	
	
	
}
