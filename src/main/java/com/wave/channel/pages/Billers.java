package com.wave.channel.pages;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.wave.channel.util.NrcInfo;
import com.wave.channel.util.TestUtil;
import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;


public class Billers extends TestBase {

	public Billers() {
		PageFactory.initElements(driver, this);
	}	

	@FindBy(id = "input_biller_field")
	public List<WebElement> Txt_field;	

	@FindBy(id="region_code")
	public WebElement nrc_region_code;	

	@FindBy(id="township_code")
	public WebElement nrc_township_code;	

	@FindBy(id="nrc_type")
	public WebElement nrc_type;

	@FindBy(id="edit_text_nrc_id")
	public WebElement nrc_id;		

	@FindBy(id = "btn_biller_continue")
	public WebElement btn_Continue;		

	@FindBy(id="btn_initial_continue")
	public WebElement btn_Confrim;	

	@FindBy(id="btn_success_finish")
	public WebElement btn_Success;		

	@FindBy(id="textinput_error")
	public List<WebElement> txt_ValidateError;	

	@FindBy(id="touch_outside")
	public WebElement select_DropDownItem;

	@FindBy(className ="android.widget.TextView")
	public List<WebElement> txt_Summary;

	public String expectedResult_Success = "Successful";

	String currentBillerAmount = "";

	String tempValue = "";



	// enter data in input fields
	public void fill_Data_ByIndex(List<WebElement> txt_field2, int index, String input, String msg) 
	{
		logger.info("Enter valid value to Input Field");

		txt_field2.get(index).clear();
		txt_field2.get(index).sendKeys(input);		
	}


	// enter data in input fields and chcek validation error
	public void fill_Data_ByIndex2(List<WebElement> txt_field2, int index, String input, String msg) {

		logger.info("Check Validation Message of Input Field : " +  msg);

		int size = txt_field2.size();
		txt_field2.get(index).clear();
		txt_field2.get(index).sendKeys(input);

		index++;
		TestUtil.sleepTime(1000);

		if (index == size) {
			TestUtil.sleepTime(500);
			int temp = 0;
			new Actions(driver).moveToElement(txt_field2.get(temp)).click().perform();
			new Actions(driver).moveToElement(txt_field2.get(size-1)).click().perform();
			TestUtil.visibilityEleWaitByLocator(By.id("textinput_error"));

		}else
		{
			TestUtil.press_Enter();		
			TestUtil.sleepTime(500);

		}
	}


	// get validation error of input field
	public String get_ValidationError(List<WebElement> txt_ValidateError2 , int index) {

		String err_validation =  txt_ValidateError2.get(index).getAttribute("text");

		return err_validation;
	}

	// select NRC Region
	public void select_NRC_RegionCode(String region_code) {			
		nrc_region_code.click();
		TestUtil.sleepTime(2000);
		TestUtil.scroll2Element(region_code);		
		select_DropDownItem.click();
	}	

	// select NRC Township
	public void select_NRC_TownshipCode(String township_code) {		
		nrc_township_code.click();
		TestUtil.sleepTime(2000);
		TestUtil.scroll2Element(township_code);
		select_DropDownItem.click();
	}	

	// select NRC Type
	public void select_NRC_Type(String type) {		
		nrc_type.click();
		TestUtil.sleepTime(2000);
		TestUtil.scroll2Element(type);
		select_DropDownItem.click();
	}

	// enter NRC ID
	public void enter_NRC_ID(String id) {		
		nrc_id.sendKeys(id);
	}

	// fill data in each input field
	int textKeyIndex = 0;
	public void fillInput (JSONArray fieldDetails) {
		// Fill textbox
		textKeyIndex = 0;

		int size = fieldDetails.length();
		for (int i = 0; i < size ; i++) {


			JSONObject fieldDetail = (JSONObject) fieldDetails.get(i);
			String key = (String) fieldDetail.get("key");


			JSONObject inputType = (JSONObject) fieldDetail.get("input-type");
			String hint = "";
			if (fieldDetail.has("hint")) {
				hint = (String) fieldDetail.get("hint");				
			}

			String value = "";
			if (fieldDetail.has("value")) {
				value = fieldDetail.getString("value");					

				tempValue = value;

			}

			String message = "Enter " + hint;

			String invalidValue = "~~~ ~~";
			String invalidValueFromJson = null;
			if (fieldDetail.has("invalid-value")) {
				invalidValueFromJson = fieldDetail.getString("invalid-value");

			}
			String type="";

			type = inputType.getString("type");

			switch (type) {
			case "number":
			case "text":


				if (key.equals("amount")) {
					currentBillerAmount = value;
					invalidValue = invalidValueFromJson!=null? invalidValueFromJson: "-2";	

				}
				if (type.equals("number")) {
					invalidValue = invalidValueFromJson!=null? invalidValueFromJson: "0";

				}
				//First test negative test (Validation messages)
				if( fieldDetail.has("validation-error-message"))
				{
					// fill invalid data
					try {
						fill_Data_ByIndex2(Txt_field,textKeyIndex, invalidValue, hint);       					

						//Error occur just one time at a time, so it is always 0
						String actualErrorMessage = get_ValidationError(txt_ValidateError, 0); 
						String expectedErrorMessage = fieldDetail.getString("validation-error-message");

						// softassert.assertEquals(actualErrorMessage, expectedErrorMessage);

						Assert.assertEquals(actualErrorMessage, expectedErrorMessage);

						test.log(Status.INFO, "Validation of field " + hint + " is : " +  actualErrorMessage );


					}catch(Exception e) {
						logger.info(e);
						e.printStackTrace();	
						size = size + 1;
						TestUtil.no_element_err();
					}

				}

				// fill valid data				
				fill_Data_ByIndex(Txt_field,textKeyIndex, value, message);
				textKeyIndex++;

				break;  

			case "NRC":			

				try {
					NrcInfo nrc = NrcInfo.getNRCInfo(value);				
					select_NRC_RegionCode(nrc.getRegion());
					select_NRC_TownshipCode(nrc.getTownship());
					select_NRC_Type(nrc.getNrcType());
					enter_NRC_ID(nrc.getNrcNo());

				} catch (Exception e) {
					//	utils.log("Invalid NRC format " + value);
				}
				break;

			case "informationBox":
				// Do nothing
				break;
			}

			driver.hideKeyboard();
			TestUtil.sleepTime(1000);
		}

	}

	// Click Continue button on Summary Screen
	public void click_Continue() {

		logger.info("Summary Screen is navigated.");
		TestUtil.visibilityEleWaitByLocator(By.id("btn_biller_continue"));
		btn_Continue.click();
		TestUtil.sleepTime(1000);

		//// To get all text in Summary page
		TestUtil.visibilityEleWaitByLocator(By.id("btn_initial_continue"));		
		String temp_txt  = get_Text();
		logger.info("Summary screen contain : " + temp_txt );

	}


	// click Confirm button on Successful Payment screen
	public void click_Confirm() 
	{
		logger.info("Successful screen is navigated.");

		TestUtil.visibilityEleWaitByLocator(By.id("btn_initial_continue"));
		btn_Confrim.click();		
		TestUtil.sleepTime(1000);

		//// To get all text in Successful Payment page
		TestUtil.visibilityEleWaitByLocator(By.id("btn_Success"));
		String temp_txt  = get_Text();
		logger.info("Successful screen contain : " + temp_txt );	

		// To get TID from Successful screen and add to report
		get_Tid();

	}

	    // Get the result text 'Successful' from successful payment screen
		public String get_result() {
			String result = txt_Summary.get(0).getText();
			return result;
		}


	// Get TID and add to Report 
	public void get_Tid() {
		int size = txt_Summary.size();
		int tid_index = size - 3;
		String tid = txt_Summary.get(tid_index).getText();
		test.log(Status.INFO, "TID : " + tid);
		logger.info("Transaction ID is : " + tid);	
	}

	// Get all text on page
	public String get_Text() {			

		String temp = "";

		for(int i=0; i < txt_Summary.size();i++) {
			temp += txt_Summary.get(i).getText();
			temp += " ";			
		}		
		return temp;			
	}

	// Open biller 
	public void open_Biller(String billerName) {

		logger.info("Open Biller : " + billerName);			
		TestUtil.sleepTime(2000);	
		driver.findElement(By.xpath ("//*[@text=\"" + billerName + "\"]")).click();
		TestUtil.sleepTime(4000);

	}


	// Calculate balance deduction and commission
	public void check_BalanceDeduction(String b_bal, String a_bal, String fee, String comm) {

		TestUtil.sleepTime(1000);

		logger.info("Calculate Balance Deduction with amount , fee and commission ");

		int val_before = TestUtil.ChangeStr2Int(b_bal);	
		logger.info("Before balance :" + val_before);
		int val_after = TestUtil.ChangeStr2Int(a_bal);
		logger.info("After balance :" + val_after);
		int amount= TestUtil.ChangeStr2Int(tempValue);	
		logger.info("Payment amount :" + amount);
		int feeAmt = TestUtil.ChangeStr2Int(fee);	
		logger.info("Fee :" + feeAmt);
		double comAmt = TestUtil.ChangeStr2Double(comm);	
		logger.info("Fee :" + comAmt);

		double val_result= (val_before - (amount+feeAmt)) + comAmt;

		logger.info("Formula is (val_before - (amount+feeAmt)) + comAmt  >> " + val_result);


		int result = (int)val_result;	

		if (val_after == result || val_after == result + 1 ) {


			String cal_result = "Before Balance : " + val_before + " Ks ; Amount : " + amount + " Ks ; Fee : " + feeAmt + " Ks ; Commission :" + comAmt + " Ks ; After Balance :" + val_after + " Ks";
			test.log(Status.INFO, cal_result);		
			logger.info("The calculated result : " + cal_result + " is same as current wallet balance : " + val_after + " , so balance calculation is correct");

		}else {
			test.log(Status.FAIL, "Balance calcuation is incorrect.");
			logger.error("Balance calcuation is incorrect.");
		}


	}


}
