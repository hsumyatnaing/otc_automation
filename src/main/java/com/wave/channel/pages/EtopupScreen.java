package com.wave.channel.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.jayway.jsonpath.JsonPath;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.DataUtil;
import com.wave.channel.util.TestUtil;

public class EtopupScreen extends TestBase {

	public EtopupScreen() {
		PageFactory.initElements(driver, this);
	}	

	String Topup_Data = DataUtil.get_Data();	
	public String telenor = (String)JsonPath.read(Topup_Data,"$.TopupData.Telenor");
	public String mpt = (String)JsonPath.read(Topup_Data,"$.TopupData.MPT");
	public String ooredoo = (String)JsonPath.read(Topup_Data,"$.TopupData.Ooredoo");
	public String fee =(String)JsonPath.read(Topup_Data,"$.TopupData.fee"); 	
	public String telnorcom =(String)JsonPath.read(Topup_Data,"$.TopupData.telenorCom"); 
	public String mptCom = (String)JsonPath.read(Topup_Data,"$.TopupData.mptCom"); 
	public String ooredooCom = (String)JsonPath.read(Topup_Data,"$.TopupData.ooredooCom"); 
	public String mptPackCom = (String)JsonPath.read(Topup_Data,"$.TopupData.mptPackCom"); 	
	public String mptPackamt = (String)JsonPath.read(Topup_Data,"$.TopupData.mptPackamt"); 
	public String telenorPackamt = (String)JsonPath.read(Topup_Data,"$.TopupData.telenorPackamt"); 	
	public String telenorPackCom = (String)JsonPath.read(Topup_Data,"$.TopupData.telenorPackCom"); 
    
	public String amt = ""; 

	public int index = 10;
	public int datapackIndex = 13;
	
	@FindBy (id="txt_title")
	public WebElement pg_title;

	@FindBy(xpath = "//*[@text=\"Mobile Topup\"]")
	public WebElement tab_MobileTopup;

	@FindBy(xpath = "//*[@text=\"Data Packs\"]")
	public WebElement tab_DataPack;

	@FindBy(id = "input_phone_number")
	public WebElement field_Input;
	
	@FindBy (className="android.widget.ImageView")
	public List<WebElement> icon_operator;	

	@FindBy (className="android.widget.RadioButton")
	public List<WebElement> rbtn_amount;

	@FindBy (id= "btn_topup_continue")
	public WebElement btn_Continue;
	
	@FindBy (id= "btn_datapack_continue")
	public WebElement btn_DataPackContinue;

	@FindBy(id="btn_initial_continue")
	public WebElement btn_Confirm;	

	@FindBy(xpath="//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]")
	public WebElement btn_back;

	@FindBy(id="btn_success_finish")
	public WebElement btn_Finish;


	@FindBy(id="btn_error_try_again")
	public WebElement btn_TryAgain;
	
	By status = By.id("txt_transaction_status");

	@FindBy(className ="android.widget.TextView")
	public List<WebElement> txt_Summary;

	public void select_Operator(int index) {
		icon_operator.get(index).click();
		TestUtil.sleepTime(1000);
	}
	
	public void fill_Number(String num) {
		field_Input.sendKeys(num);
		TestUtil.sleepTime(1000);
		driver.hideKeyboard();
		TestUtil.sleepTime(1000);
	}

	public void choose_TopupAmount() {
		amt = rbtn_amount.get(0).getText();		
		TestUtil.sleepTime(1000);
		rbtn_amount.get(0).click();		
		TestUtil.sleepTime(1000);
	}
	
	public void choose_DataPack() {		
		rbtn_amount.get(0).click();		
		TestUtil.sleepTime(1000);
	}


	public void click_Continue() {
		btn_Continue.click();
		TestUtil.sleepTime(1000);
	}
	
	public void click_DataPackContinue() {
		btn_DataPackContinue.click();
		TestUtil.sleepTime(1000);
	}

	public void click_Confrim() {
		btn_Confirm.click();
		TestUtil.sleepTime(1000);
		TestUtil.visibilityEleWaitByLocator(status);
	}

	public void click_Finish() {
		btn_Finish.click();
		TestUtil.sleepTime(1000);
	}

	public void click_TryAgain() {
		btn_TryAgain.click();
		TestUtil.sleepTime(1000);
	}
	
	public void backto_Home() {
		btn_back.click();
		TestUtil.sleepTime(1000);
	}

	public String get_result() {
		String result =txt_Summary.get(0).getText();
		return result;
	}

	public void open_DataPacksTab() {
		tab_DataPack.click();
		TestUtil.sleepTime(1000);
	}

}
