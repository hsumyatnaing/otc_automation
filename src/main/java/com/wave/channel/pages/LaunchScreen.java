package com.wave.channel.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.TestUtil;

public class LaunchScreen extends TestBase {
	
    // MSISND field
	@FindBy(id = "input_msisdn")
	public WebElement txt_input;

	// Continue button
	@FindBy(id = "btn_register_continue")
	public WebElement btn_continue;	

	// Change language icon
	@FindBy (id = "toolbar_right_icon")
	public WebElement btn_change_lang;

	// Language drop-down
	@FindBy(id="language_text") 
	public List<WebElement> lst_language;
	
	public LaunchScreen() {		
		PageFactory.initElements(driver, this);
	}
	
	public void changeENG()  {	
		
		logger.info("Change App Language to English");
		
		TestUtil.sleepTime(1000);
		btn_change_lang.click();	
		TestUtil.sleepTime(1000);
		lst_language.get(0).click();
		TestUtil.sleepTime(1000);
	}
	
	public void changeUni() throws InterruptedException {
		
		logger.info("Change App Language to Unicode");
		
		TestUtil.sleepTime(1000);
		btn_change_lang.click();	
		TestUtil.sleepTime(1000);
		lst_language.get(1).click();
		TestUtil.sleepTime(1000);
	}
	
	public void changeZawgyi() throws InterruptedException {
		
		logger.info("Change App Language to Zawgyi");
		
		TestUtil.sleepTime(1000);
		btn_change_lang.click();	
		TestUtil.sleepTime(1000);
		lst_language.get(2).click();
		TestUtil.sleepTime(1000);
	}
	
	public void enterMSISDN(String MSISDN) {
		
		logger.info("Enter Agent Phone Number");
		test.log(Status.INFO, "Enter Agent Phone Number");
		TestUtil.sleepTime(1000);
		txt_input.sendKeys(MSISDN); 
		TestUtil.sleepTime(500);			
	}
	
	public PinScreen gotoPINScreen() {	
		
		logger.info("Go to PIN Screen");
		test.log(Status.INFO, "Navigate to PIN Screen");		
		
		
		btn_continue.click();
	    TestUtil.sleepTime(1000);
		By loginB = By.id("btn_login");
	    WebDriverWait wait = new WebDriverWait(driver, 60);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(loginB));
		} catch (TimeoutException e) {
			e.getMessage();
		}
		return new PinScreen();
		
	}

}
