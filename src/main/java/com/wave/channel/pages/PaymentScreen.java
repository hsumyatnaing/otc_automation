package com.wave.channel.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wave.channel.base.TestBase;
import com.wave.channel.util.TestUtil;

public class PaymentScreen extends TestBase {
	
	public PaymentScreen() 
	{	
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="txt_title")
	public WebElement title;	
	
	@FindBy(xpath = "//*[@text=\"Foodpanda\"]")
	public WebElement icon_Foodpanda;
	
	@FindBy(xpath = "//*[@text=\"AEON\"]")
	public WebElement icon_AEON;	
	
	@FindBy(xpath = "//*[@text=\"Unilink\"]")
	public WebElement icon_Unilink;
	
	@FindBy(xpath = "//*[@text=\"Mahar Bawga (Yangon)\"]")
	public WebElement icon_MaharBawgaYGN;
	
	@FindBy(xpath = "//*[@text=\"Yoma Bank\"]")
	public WebElement icon_YomaBank;
	
	@FindBy(xpath = "//*[@text=\"mBuyy\"]")
	public WebElement icon_mBuyy;
	
	@FindBy(xpath="//*[@text=\"Sun King Solar\"]")
	public WebElement icon_SunKing;
	
	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]")
	public WebElement btn_back;
	
	@FindBy(id="mm.com.wavemoney.mfs.wavepartner.preprod:id/txt_item_name")
	public List<WebElement> biller_name;
	
	
//	public void openBiller(WebElement billerName) {
//		TestUtil.sleepTime(1000);	
//		billerName.click();
//		TestUtil.sleepTime(1000);		
//	}
	
	
	public void openBiller(WebElement billerName) {
		TestUtil.sleepTime(1000);	
		billerName.click();
		TestUtil.sleepTime(1000);		
	}
	

	public void back2Home() {
		btn_back.click();
	}
	
	
	
	
	
	
	
}
