package com.wave.channel.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.jayway.jsonpath.JsonPath;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.DataUtil;
import com.wave.channel.util.TestUtil;

import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class SendMoneyScreen extends TestBase {
	
	public SendMoneyScreen() {	
		PageFactory.initElements(driver, this);
	}	
	public String expectedResult_Success = "Successful";
	
	String Test_Data = DataUtil.get_Data();	
	public String senderPhno = (String)JsonPath.read(Test_Data,"$.SendMoney.senderPhno");
	public String senderName = (String)JsonPath.read(Test_Data,"$.SendMoney.senderName");
	public String senderNRC = (String)JsonPath.read(Test_Data,"$.SendMoney.senderNRC");
	public String OTCPhno = (String)JsonPath.read(Test_Data,"$.SendMoney.OTCPhno");
	public String MAPhno = (String)JsonPath.read(Test_Data,"$.SendMoney.MAPhno");	
	public String receiverName = (String)JsonPath.read(Test_Data,"$.SendMoney.receiverName"); 
	public String receiverNRC = (String)JsonPath.read(Test_Data,"$.SendMoney.receiverNRC");
	public String amount = (String)JsonPath.read(Test_Data,"$.SendMoney.amount");
	public String fee = (String)JsonPath.read(Test_Data,"$.SendMoney.fee");
	public String com = (String)JsonPath.read(Test_Data,"$.SendMoney.com");
	public String partnerPhno = (String)JsonPath.read(Test_Data,"$.SendMoney.partnerPhno");
	
	String Error_Msg = DataUtil.get_Error();
	public String err_sender = (String)JsonPath.read(Error_Msg,"$.SendMoney.err_sender");
	public String err_receiver = (String)JsonPath.read(Error_Msg,"$.SendMoney.err_receiver");
	public String err_nonWA = (String)JsonPath.read(Error_Msg,"$.SendMoney.err_nonWA");
	public String err_samePOI = (String)JsonPath.read(Error_Msg,"$.SendMoney.err_samePOI");	
	public String err_insufBal = (String)JsonPath.read(Error_Msg,"$.CommonErr.err_insufBal");	
	

	public int indexOTC= 24;	
	public int indexMA= 22;
	
	@FindBy(id="txt_title")
	public WebElement title;	
	
	@FindBy(xpath = "//*[@text=\"To Wave Shop\"]")
	public WebElement tab_OTC;
	
	@FindBy(xpath = "//*[@text=\"To Wave Account\"]")
	public WebElement tab_MA;	
	
	@FindBy(className ="android.widget.EditText")
	public List<WebElement> Txt_field;	
		
	@FindBy(id = "btn_continue")
	public WebElement btn_Continue;	
	
	@FindBy(id="btn_initial_continue")
	public WebElement btn_Confrim;
	
	@FindBy(className ="android.widget.TextView")
	public List<WebElement> txt_Summary;	
	
	@FindBy(id="btn_error_try_again")
	public WebElement btn_TryAgain;	
	
	@FindBy(xpath="//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]")
	public WebElement btn_back;
	
	@FindBy(id="txt_error")
	public WebElement txt_Error;	
	
	@FindBy(xpath="(//android.widget.ImageView[@content-desc=\"WC PreProd\"])[4]")
	public WebElement btn_dropdown;
	
	@FindBy(id="title_choose_id_type")
	public WebElement txt_idTitle;	
	
	@FindBy(id="title_driving_license")
	public WebElement txt_titleDrivingLicense;
	
	@FindBy(id="title_passport")
	public WebElement txt_titlePassport;	
	
	@FindBy(id="container")
	public WebElement id_container;
	
	
	public By btnFinish = By.id("btn_success_finish");
	
	
	
	public String getTitle() {	
		return title.getText();
	}
	
	public void fill_data_OTC(String sNo,String sName,String sNRC,String rNo,String rName,String rNRC,String amt) {
		
		Txt_field.get(0).sendKeys(sNo);	
		Txt_field.get(1).sendKeys(sName);
		Txt_field.get(2).sendKeys(sNRC);		
		Txt_field.get(3).sendKeys(rNo);
		TestUtil.press_Enter();		
		Txt_field.get(4).sendKeys(rName);
		TestUtil.press_Enter();				
		Txt_field.get(5).sendKeys(rNRC);
		TestUtil.press_Enter();			
		Txt_field.get(6).sendKeys(amt);
		TestUtil.press_Enter();			
		driver.hideKeyboard();	
		
		TestUtil.visibilityEleWaitByLocator(By.id("btn_continue"));
		
	}	
	
	
//	public void fill_data_OTC_2(String sNo,String sName,String sNRC,String rNo,String rName,String rNRC,String amt) {
//		
//		secretCode_OTC = TestUtil.getRandomNumberString();
//		
//		Txt_field.get(0).sendKeys(sNo);	
//		Txt_field.get(1).sendKeys(sName);
//		Txt_field.get(2).sendKeys(sNRC);		
//		Txt_field.get(3).sendKeys(rNo);
//    	TestUtil.press_Enter();	
//		driver.hideKeyboard();
//		Txt_field.get(4).sendKeys(rName);
//    	TestUtil.press_Enter();		
//		driver.hideKeyboard();
//		Txt_field.get(5).sendKeys(rNRC);
//        TestUtil.press_Enter();	
//		driver.hideKeyboard();
//		Txt_field.get(6).sendKeys(amt);
//		TestUtil.press_Enter();	
//		driver.hideKeyboard();	
//		
//		TestUtil.swipe(529, 1605, 630, 784);
//		TestUtil.sleepTime(1000);
//				
//	    	
//		Txt_field.get(5).sendKeys(secretCode_OTC);
//		driver.hideKeyboard();
//		
//		TestUtil.visibilityEleWaitByLocator(By.id("btn_continue"));
//		
//	}	
	
	public void fill_data_MA(String sPhno, String sName, String sNRC,String rPhno,String amt) {
		
	 	Txt_field.get(0).sendKeys(sPhno);	
		Txt_field.get(1).sendKeys(sName);
		Txt_field.get(2).sendKeys(sNRC);	
		Txt_field.get(3).sendKeys(rPhno);	
		TestUtil.press_Enter();
		Txt_field.get(4).sendKeys(amt);	
		driver.hideKeyboard();
		TestUtil.sleepTime(1000);
		
	}

	public void click_Continue() {
		btn_Continue.click();
		TestUtil.visibilityEleWaitByLocator(By.className("android.widget.Button")); // 2
	}

	public void click_Confirm() {
		btn_Confrim.click();
		TestUtil.visibilityEleWaitByLocator(By.className("android.widget.Button")); // 1
	}
	
	public void open_MAtab() {
		tab_MA.click();
		TestUtil.sleepTime(1000);
	}	
	
	public void open_OTCtab() {
		tab_OTC.click();
		TestUtil.sleepTime(1000);
	}
		
//	public void click_TryAgain() {
//		btn_TryAgain.click();
//		TestUtil.sleepTime(1000);
//	}
	
//	public void backto_Home() {
//		btn_back.click();
//		TestUtil.sleepTime(1000);
//	}
//	
	public String get_result() {
		TestUtil.sleepTime(500);
		String result =txt_Summary.get(0).getText();
		return result;
	}	
	
}
