package com.wave.channel.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.jayway.jsonpath.JsonPath;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.DataUtil;
import com.wave.channel.util.TestUtil;

public class ReceiveMoneyScreen extends TestBase {

	public ReceiveMoneyScreen() {	
		PageFactory.initElements(driver, this);
	}

	String Test_Data = DataUtil.get_Data();	
	public String tid = (String)JsonPath.read(Test_Data,"$.ReceiveMoney.tidMA2OTC");
	public String invalidtid = (String)JsonPath.read(Test_Data,"$.ReceiveMoney.invalidtid");	
	
	public String pin = (String)JsonPath.read(Test_Data,"$.ReceiveMoney.pin");
	public String invalidpin = (String)JsonPath.read(Test_Data,"$.ReceiveMoney.invalidpin");
	
	public String amt = (String)JsonPath.read(Test_Data,"$.ReceiveMoney.amt");
	public String fee = (String)JsonPath.read(Test_Data,"$.ReceiveMoney.fee"); 
	public String com = (String)JsonPath.read(Test_Data,"$.ReceiveMoney.com");
	
	String Error_Msg = DataUtil.get_Error();
	
	public String err_invalidTID = (String)JsonPath.read(Error_Msg,"$.ReceiveMoney.err_invalidTID");
	public String err_invalidPIN = (String)JsonPath.read(Error_Msg,"$.ReceiveMoney.err_invalidPIN");
	
	public int index= 7;

	@FindBy(id="txt_title")
	public WebElement title;

	@FindBy(className ="android.widget.EditText") 
	public List<WebElement> Txt_field;

	@FindBy(id = "btn_continue")
	public WebElement btn_Continue;	

	@FindBy(id="btn_initial_continue")
	public WebElement btn_Confrim;

	@FindBy(className ="android.widget.TextView")
	public List<WebElement> txt_Summary;
	//btn_success_finish

	public void fill_data(String tid , String pin) {
		Txt_field.get(0).sendKeys(tid);
		TestUtil.sleepTime(1000);
		Txt_field.get(1).sendKeys(pin);
		TestUtil.sleepTime(1000);
		driver.hideKeyboard();		
	}

	public void click_Continue() {
		btn_Continue.click();
		TestUtil.sleepTime(1000);
	}

	public void click_Confirm() {
		btn_Confrim.click();
		TestUtil.sleepTime(1000);
	}

	public String get_result() {
		String result =txt_Summary.get(0).getText();
		return result;
	}



}
