package com.wave.channel.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.jayway.jsonpath.JsonPath;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.DataUtil;
import com.wave.channel.util.TestUtil;

public class PartnerScreen extends TestBase {

	public PartnerScreen() {
		PageFactory.initElements(driver, this);	}
	
//	public String expectedResult_Success = "Successful";	
	
	String data = DataUtil.get_Data();
	public String FoFo = (String)JsonPath.read(data,"$.PayPartner.FOFO");
	public String CSE = (String)JsonPath.read(data,"$.PayPartner.CSE");
	public String amt = (String)JsonPath.read(data,"$.PayPartner.amt");
	public String fee = (String)JsonPath.read(data,"$.PayPartner.fee");
	public String com = (String)JsonPath.read(data,"$.PayPartner.com");		
	
	String err = DataUtil.get_Error();
	public String expectedResult_Fail = (String)JsonPath.read(err, "$.PayPartner.err_nonPartner");
	
	public int label= 9;
	public int index= 10;

	
	@FindBy(xpath = "//*[@text=\"Bank Transfer\"]")
	public WebElement icon_BankTransfer;

	@FindBy(xpath = "//*[@text=\"Pay partner\"]")
	public WebElement icon_PayPartner;

	@FindBy(className ="android.widget.EditText") 
	public List<WebElement> Txt_field;
	
	@FindBy(id = "btn_continue")
	public WebElement btn_Continue;	
	
	@FindBy(id = "btn_initial_continue")
	public WebElement btn_Confirm;	

	@FindBy(className ="android.widget.TextView")
	public List<WebElement> txt_Summary;

	public void open_BankTransfer() {
		icon_BankTransfer.click();
		TestUtil.sleepTime(1000);	}

	public void open_PayPartner() {
		icon_PayPartner.click();
		TestUtil.sleepTime(1000);
	}
		
	public void click_Continue() {
		btn_Continue.click();
		TestUtil.sleepTime(1000);
	}
	
	public void click_Confirm() {
		btn_Confirm.click();
		TestUtil.sleepTime(1000);
	}

	public void do_Payment(String msisdn,String amt) 
	{		
		TestUtil.sleepTime(1000);
		Txt_field.get(0).sendKeys(msisdn);
		Txt_field.get(1).sendKeys(amt);		
		driver.hideKeyboard();
		TestUtil.sleepTime(1000);
		click_Continue();
		TestUtil.sleepTime(1000);
		click_Confirm();
		TestUtil.sleepTime(1000);
	}

	public String get_result() {
		String result =txt_Summary.get(0).getText();
		return result;
	}
//	public void get_TID() {		
//		String name = "Paypartner";
//		String label=txt_Summary.get(9).getText(); 
//		String tid=txt_Summary.get(10).getText();
//		DataUtil.wirteData(name,label,tid);
//		test.log(Status.INFO, MarkupHelper.createCodeBlock(DataUtil.readData(), CodeLanguage.JSON));
//	}
}
