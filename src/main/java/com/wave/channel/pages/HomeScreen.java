package com.wave.channel.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.wave.channel.base.TestBase;
import com.wave.channel.util.TestUtil;


public class HomeScreen extends TestBase{	
	
	public HomeScreen() 
	{	
		PageFactory.initElements(driver, this);
	}
	
	public String expectedResult_Success = "My Balance";
	
	@FindBy(id="balance_text")
	public WebElement my_balance;

	@FindBy(id="home_balance")
	public WebElement wave_balance;	
		
	@FindBy(xpath = "//*[@text=\"Send Money\"]")
	public WebElement icon_SendMoney;
	
	@FindBy(xpath = "//*[@text=\"Receive Money\"]")
	public WebElement icon_ReceiveMoney;	
	
	@FindBy(xpath = "//*[@text=\"Cash In\"]")
	public WebElement icon_CashIn;
	
	@FindBy(xpath = "//*[@text=\"Cash Out\"]")
	public WebElement icon_CashOut;
	
	@FindBy(xpath = "//*[@text=\"E-topup\"]")
	public WebElement icon_Etopup;
	
	@FindBy(xpath = "//*[@text=\"Payment\"]")
	public WebElement icon_Payment;
	
	@FindBy(xpath = "//*[@text=\"Bank Service\"]")
	public WebElement icon_BankService;
	
	@FindBy(xpath = "//*[@text=\"WavePay Registration\"]")
	public WebElement icon_WavePayReg;
	
	@FindBy(xpath = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Customer\"]/android.widget.TextView")
	public WebElement tab_Customer;
	
	@FindBy(xpath = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Partner\"]/android.widget.TextView")
	public WebElement tab_Partner;
	
	@FindBy(xpath = "//*[@text=\"History\"]")
	public WebElement icon_History;
    
	@FindBy(className ="android.widget.ImageButton")
	public WebElement menu_hamburger;	

	@FindBy(xpath = "//*[@text=\"Scan QR\"]")
	public WebElement btn_ScanQR;	
	
	@FindBy(xpath = "//*[@text=\"Wave Shops\"]")
	public WebElement btn_WaveShops;		

	@FindBy(xpath = "//*[@text=\"Inbox\"]")
	public WebElement btn_Inbox;	
	
	public String getTitle() {		
		return my_balance.getText();		
	}
	
	public String getBalance() {		
		TestUtil.sleepTime(1500);
		String balance= wave_balance.getText();
		return balance;	
	}
	
	public void open_HamburgerMenu() {
		TestUtil.sleepTime(500);
		menu_hamburger.click();
		TestUtil.sleepTime(500);
	}

	public SendMoneyScreen goToSendMoney() {	
		TestUtil.sleepTime(500);
		icon_SendMoney.click();
		TestUtil.sleepTime(1000);
		return new SendMoneyScreen();
	}

	public ReceiveMoneyScreen goToReceiveMoney() {		
		icon_ReceiveMoney.click();
		TestUtil.sleepTime(1000);
		return new ReceiveMoneyScreen();
	}
		
	public CashInScreen gotoCashIn() {
		icon_CashIn.click();
		TestUtil.sleepTime(1000);
		return new CashInScreen();
	}
	
	public CashOutScreen gotoCashOut() {
		TestUtil.sleepTime(1000);
		icon_CashOut.click();
		TestUtil.sleepTime(1000);
		return new CashOutScreen();
	}	
	
	public EtopupScreen gotoEtopup() {
		TestUtil.sleepTime(1000);
		icon_Etopup.click();
		TestUtil.sleepTime(1000);
		return new EtopupScreen();
	}
	
	public PaymentScreen gotoPayment() {
		
		logger.info("Open Payment Screen");
		
		TestUtil.sleepTime(500);
		icon_Payment.click();
		TestUtil.sleepTime(1000);
		return new PaymentScreen();
	}
	
	public BankServiceScreen gotoBankService() {
		icon_BankService.click();
		TestUtil.sleepTime(1000);
		return new BankServiceScreen();
	}
	
	public WPRegScreen gotoWpReg() {
		icon_WavePayReg.click();
		TestUtil.sleepTime(1000);
		return new WPRegScreen();
	}
	
	public HomeScreen openCustomerTab() {
	    tab_Customer.click();
		TestUtil.sleepTime(1000);
		return new HomeScreen();
    }
	
    public PartnerScreen openParnterTab() {
    	tab_Partner.click();
		TestUtil.sleepTime(1000);
		return new PartnerScreen();
    }
	
    public void openHistory() {
    	icon_History.click();
    	TestUtil.sleepTime(1000);
    }	
    
    public void openScanQR() {
    	btn_ScanQR.click();
    	TestUtil.sleepTime(1000);
    }
	
    public void openWaveShops() {
    	btn_WaveShops.click();
    	TestUtil.sleepTime(1000);
    }
    
    public void openInbox() {
    	btn_Inbox.click();
    	TestUtil.sleepTime(1000);    	
    }
	public String Get_Balance() {	
		
		logger.info("Get Current Wallet Balance");	
		////////////////////////////////////////////////////////////
		TestUtil.visibilityEleWaitByLocator(By.id("balance_text")); 
		TestUtil.sleepTime(1000);		
		String balance= wave_balance.getText();
		return balance;
	}
}
