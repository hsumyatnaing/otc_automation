package com.wave.channel.util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class ListenerUtil extends TestBase implements ITestListener {


	ExtentReports report = ReportUtil.setupReport();

	
	
	public void onTestStart(ITestResult result) {		
	   
		test = report.createTest(result.getName());		
		
	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		test.log(Status.PASS,"Pass");	
		
	}

	public void onTestFailure(ITestResult result) {
		test.fail(result.getThrowable());

	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
     	test.skip(result.getTestName());	


	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		report = ReportUtil.setupReport();
	}
	
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		report.flush();
	}



}
