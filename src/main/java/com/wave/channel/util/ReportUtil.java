package com.wave.channel.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ReportUtil {
	
	static ExtentReports report;
	static ExtentSparkReporter reporter;

	public static ExtentReports setupReport() {
		report = new ExtentReports();
		String fileName = new SimpleDateFormat("yyyyMMddHHmm'.html'").format(new Date());		
		String path = System.getProperty("user.dir")+"\\Reports\\" + fileName;
		ExtentSparkReporter reporter = new ExtentSparkReporter(path);
		reporter.config().setReportName("Wave Channel Automation Test Resut");		
		reporter.config().setTheme(Theme.DARK);	
		
		report.attachReporter(reporter);
		report.setSystemInfo("OS" , "Android");
	
		return report;
	}

}
