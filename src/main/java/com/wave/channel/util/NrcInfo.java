package com.wave.channel.util;


public class NrcInfo {
	String region;
    String township;
    String nrcType;
    String nrcNo;

    public NrcInfo(String region, String township, String nrcType, String nrcNo) {
        this.region = region;
        this.township = township;
        this.nrcType = nrcType;
        this.nrcNo = nrcNo;
    }

    
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTownship() {
        return township;
    }

    public void setTownship(String township) {
        this.township = township;
    }

    public String getNrcType() {
        return nrcType;
    }

    public void setNrcType(String nrcType) {
        this.nrcType = nrcType;
    }

    public String getNrcNo() {
        return nrcNo;
    }

    public void setNrcNo(String nrcNo) {
        this.nrcNo = nrcNo;
    }
    
    public static NrcInfo getNRCInfo(String nrc)
    {
        
        String region = nrc.substring(0, nrc.indexOf("/"));        
       
        
        String township = nrc.substring(nrc.indexOf("/")+1,nrc.indexOf("("));        
               
        
        String type = nrc.substring(nrc.indexOf("(")+1,nrc.indexOf(")"));        
     
        
        String no = nrc.substring(nrc.indexOf(")")+1);
       
        
        NrcInfo info = new NrcInfo(region,township,type,no);
        
        return info;       
    }
    
    @Override
    public String toString() {
        return "NRCInfo{" + "region=" + region + ", township=" + township + ", nrcType=" + nrcType + ", nrcNo=" + nrcNo + '}';
    }
}
