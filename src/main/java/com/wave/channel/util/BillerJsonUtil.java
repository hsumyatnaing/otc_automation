package com.wave.channel.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.wave.channel.base.TestBase;

public class BillerJsonUtil extends TestBase {
	
	public JSONObject getBillerJson(String filePath)
	{
		String path = "data/billers/"+filePath;
		InputStream datais = null;
		datais = getClass().getClassLoader().getResourceAsStream(path);
		JSONTokener tokener = new JSONTokener(datais);
		
		JSONObject billerJson;
    	billerJson = new JSONObject(tokener);		
		
		return billerJson;
		
	}
	
	//Read biller one by one
	public Object[][] getBillerTestData()
	{
		Object[][] arr = null;
		String billerListFile = "data/biller_list.properties";
		InputStream billerInputStream = getClass().getClassLoader().getResourceAsStream(billerListFile);
		//FileInputStream fs = new FileInputStream(billerListFile);
		BufferedReader br = new BufferedReader(new InputStreamReader(billerInputStream));

		String strLine;

		int noOfJsonFile = 0;
		ArrayList<String> billerJsonFiles = new ArrayList<String>();
		try {
			while ((strLine = br.readLine()) != null)   {
			  // Print the content on the console
			  System.out.println ("File "+strLine);
			  if(!strLine.startsWith("#") && !strLine.isEmpty())
			  {
				  noOfJsonFile ++;
				  billerJsonFiles.add(strLine);
			  }
//			  	arr = new Object[billerJsonFiles.size()][2];
			 	arr = new Object[billerJsonFiles.size()][4];
				for (int i = 0; i < billerJsonFiles.size(); i++) {
					JSONObject biller = this.getBillerJson(billerJsonFiles.get(i));
					arr[i][0] = biller.get("billerName");
					arr[i][1] = biller;	
					arr[i][2] = biller.get("fee");
					arr[i][3] = biller.get("fofocomm");		// need to change here for other agent types			
				}
			}
			br.close();
			billerInputStream.close();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Close the input stream
		
		return arr;
	}
}
