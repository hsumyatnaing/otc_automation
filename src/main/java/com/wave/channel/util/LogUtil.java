package com.wave.channel.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogUtil {

	private static Logger logger = LogManager.getLogger(LogUtil.class);

	public static void main(String[] args) {

		LogUtil obj = new LogUtil();

		try{
			obj.divide();
		}catch(ArithmeticException ex){
			if(logger.isDebugEnabled()){
	            logger.debug("This is debug : test ");
	        }
	        
	        if(logger.isInfoEnabled()){
	            logger.info("This is info : tt ");
	        }
	        
	        logger.warn("This is warn : " );
	        logger.error("This is error : " );
	        logger.fatal("This is fatal : " );
	        
	    
		}

	}

	private void divide(){

//		int i = 10 /0;

	}

}
