package com.wave.channel.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.wave.channel.base.TestBase;
import com.wave.channel.pages.HomeScreen;
import com.wave.channel.pages.LaunchScreen;
import com.wave.channel.pages.PinScreen;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class TestUtil extends TestBase {

	public static String expectedResult_Success = "Successful";

	// Login Application
	public static HomeScreen loginApp() {
		LaunchScreen launchScr = new LaunchScreen();	
		launchScr.changeENG();
		launchScr.enterMSISDN(prop.getProperty("misdn"));
		PinScreen pinScr = launchScr.gotoPINScreen();
		TestUtil.sleepTime(1000);
		pinScr.enterPIN(prop.getProperty("pin"));
		TestUtil.sleepTime(3000);
		return pinScr.clickLogInBtn();		 
	}

	// Wait element visibility by locator 
	public static void visibilityEleWaitByLocator(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 45);	
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch (TimeoutException e) {
			e.getMessage();
		}
	}



	// Thread Sleep Time
	public static void sleepTime(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Vertical scroll to element
	public static void scroll2Element(String element) {
		try {

			logger.info("Scroll down to " + element);

			TestUtil.sleepTime(2000);
			// driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true))" + ".scrollIntoView(new UiSelector().text(\"" + element + "\"))"));
			// driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+element+"\").instance(0))"));

			// driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\""+element+"\").instance(0))"));

			driver.findElement(MobileBy.AndroidUIAutomator(
					"new UiScrollable(new UiSelector().scrollable(true))" +
							".scrollIntoView(new UiSelector().text(\""+ element + "\"))"));
		}catch(Exception e) {
			logger.info(e);
			e.printStackTrace();
			no_element_err();

		}


	}

	// ** String Utility **
	// remove all special characters and change to integer
	public static int ChangeStr2Int(String text) {
		String str = text.replaceAll("[^\\d]", "");	
		return Integer.parseInt(str);
	}

	public static Double ChangeStr2Double(String text) {
		String str = text.replaceAll("[^\\d.]", "");	
		return Double.parseDouble(str);
	}

	// remove all spaces in string
	public static String removeSpace(String txt) {
		String after_txt = txt.replaceAll(" ", "");
		return after_txt;
	}

	public static String extractDigitFromString(String text) {
		String str = text.replaceAll("[^0-9]", "");	
		return str;
	}


	// Balance deduct calculation
	public static void Check_BalanceDeduction(String before,String after,String amt,String fees,String com) {
		TestUtil.sleepTime(1000);
		int val_before = ChangeStr2Int(before);	
		//		System.out.println("before : " + val_before);
		int val_after = ChangeStr2Int(after);
		//	    System.out.println("after : " + val_after);
		int amount= ChangeStr2Int(amt);	
		//		System.out.println("Amt : " + amount);
		int fee=ChangeStr2Int(fees);	
		//		System.out.println("Fee : " + fee);
		double comm=ChangeStr2Double(com);	
		//		System.out.println("Comm : " + comm);
		double val_result= (val_before - (amount+fee)) + comm;
		//	    System.out.println("calculation result : " + val_result);
		int result = (int)val_result;	

		if (val_after == result || val_after == result + 1 ) {

			String cal_result = "Before Balance : " + before + "; Amount : " + amt + "; Fee : " + fees + "; POS Commission :" + com + "; After Balance :" + after;
			test.log(Status.INFO, cal_result);

		}else {
			test.log(Status.FAIL, "Balance calcuation is incorrect.");
		}
	}

	// Calculate balance deduction for Receive Money
	public static void Check_RM_Calcuation(String before,String after,String amt,String fees,String com) {
		TestUtil.sleepTime(1000);
		int val_before = ChangeStr2Int(before);
		int val_after = ChangeStr2Int(after);
		int amount= Integer.parseInt(amt);
		double comm=Double.parseDouble(com);	

		double val_result=val_before+amount+comm;
		int result = (int)val_result;

		if (val_after == result) {
			String cal_result = "Before Balance : " + before + "; Amount : " + amt + "; Fee : " + fees + "; POS Commission :" + com + "; After Balance :" + after;		
			test.log(Status.INFO, cal_result);

		}else {
			System.out.println("Fail");
		}
	}

	// Click Continue Button in Biller Screen
	//	public static void click_BillerContinue() {
	//		TestUtil.sleepTime(1000);
	//		By btn_Continue = By.id("btn_biller_continue");					
	//		TestUtil.visibilityEleWaitByLocator(btn_Continue);
	//		driver.findElement(btn_Continue).click();
	//		TestUtil.sleepTime(2000); // change back to 1000 after aung pwal
	//	}

	// Click Confirm Button in Biller Screen
	//	public static void click_BillConfirm() {
	//		By btn_Confrim = By.id("btn_initial_continue");	
	//		TestUtil.visibilityEleWaitByLocator(btn_Confrim);
	//		driver.findElement(btn_Confrim).click();		
	//		TestUtil.visibilityEleWaitByLocator(By.className("android.widget.Button"));
	//		//TestUtil.visibilityEleWaitByLocator(By.id("btn_success_finish"));
	//
	//	}

	// Click button on each Successful screen
	public static void Click_Success() {
		logger.info("Navigate back to Home screen");
		By btn_Success = By.id("btn_success_finish");	
		TestUtil.visibilityEleWaitByLocator(btn_Success);
		driver.findElement(btn_Success).click();		
		TestUtil.sleepTime(1000);
		TestUtil.visibilityEleWaitByLocator(By.id("txt_kyat"));

	}

	// Get TID in Successful screen and add in report	
	public static String get_TID(List<WebElement> sum, int tid_index) {			
		String tid = sum.get(tid_index).getText();
		String transID = "Transaction ID - " + tid;	
		test.log(Status.INFO, transID);
		TestUtil.sleepTime(1000);
		//		tid_SendMoney2OTC = tid;
		return tid;
	}

	// Get Result of the payment (e.g. Successful) 
	public static String get_Result(List<WebElement> txt_Summary) {
		String result =txt_Summary.get(0).getText();
		return result;
	}


	// Take Screenshot
	public static String Take_SS(String name) {

		visibilityEleWaitByLocator(By.id("btn_success_finish"));

		String dest="";
		try {
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String temp = timestamp() + "_" + name;
			dest = System.getProperty("user.dir") +"\\ScreenShots\\IMG_"+temp+".png";

			//			dest = System.getProperty("user.dir") +"\\ScreenShots\\IMG_"+name+".png";
			File destination = new File(dest);
			FileUtils.copyFile(source, destination); 

		}catch(Exception e) {
			e.printStackTrace();
		}
		return dest;
	}

	public static String timestamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
	}



	// Add screenshot to Report
	public static void addSS2Report(String name) {

		logger.info("Take screenshot and add to Report");
		TestUtil.sleepTime(2000);
		dest = TestUtil.Take_SS(name);
		test.log(Status.INFO, MediaEntityBuilder.createScreenCaptureFromPath(dest).build());
		TestUtil.sleepTime(2000);
	}




	// Set insufficient balance by adding 100000 to current balance
	public static String Set_InsuffientBalance(String currentBal) {		
		String str = currentBal.replaceAll("[^\\d]", "");		
		int cur_Bal = Integer.parseInt(str);	 
		int insuff_Bal = cur_Bal + 100000 ;
		return Integer.toString(insuff_Bal);

	}

	// click back arrow icon and navigate back to Home screen
	public static void back2Home() {
		By btn_backArrow = By.id("back_arrow");
		driver.findElement(btn_backArrow).click();   
		//		TestUtil.sleepTime(1000);
		TestUtil.visibilityEleWaitByLocator(By.id("txt_kyat"));
	}

	// click Try Again button on Fail screen of transaction
	public static void click_TryAgain() {
		By btn_tryagain = By.id("btn_error_try_again");
		driver.findElement(btn_tryagain).click();   
		TestUtil.sleepTime(2000);
	}

	// click back arrow icon from Payment Screen and navigate back to Home Screen
	public static void Return_HomeScreen() {     	
		By btn_back_2 = By.xpath("//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]");
		driver.findElement(btn_back_2).click();
		//		TestUtil.sleepTime(1000);
		TestUtil.visibilityEleWaitByLocator(By.id("txt_kyat")); // 3

	}


	public static void Click_HomeMenu() {
		//		driver.findElement(By.id("homeMenuFragment")).click();
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Home\"]")).click();
		TestUtil.sleepTime(1000);
		TestUtil.visibilityEleWaitByLocator(By.id("txt_kyat"));
	}


	public static void press_Enter() {
		((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
	}


	//	@SuppressWarnings("rawtypes")
	//	public static void clean_Noti() {		
	//		driver.runAppInBackground(Duration.ofSeconds(5));
	//		((AndroidDriver) driver).openNotifications();
	//		driver.findElement(By.id("com.android.systemui:id/dismiss_view")).click();
	//
	//	}

	//	@SuppressWarnings("rawtypes")
	//	public static void read_Noti() {
	//
	//		driver.runAppInBackground(Duration.ofSeconds(5));
	//
	//
	//		((AndroidDriver) driver).openNotifications();
	//		TestUtil.sleepTime(2000);
	//		String content = "";
	//		List<MobileElement> lst_title = driver.findElements(By.id("android:id/title"));		 
	//		List<MobileElement> lst_text = driver.findElements(By.id("android:id/text"));
	//
	//		//		System.out.println(lst_title.get(0).getText());
	//		//		System.out.println(lst_text.get(0).getText());
	//
	//		//		if(lst_title.get(0).getText()=="Wave Money") {
	//		//			content = lst_text.get(0).getText();
	//		//		}
	//
	//		content = TestUtil.removeSpace(lst_text.get(0).getText());
	//		System.out.println(content);
	//		driver.findElement(By.id("com.android.systemui:id/dismiss_view")).click();
	//		//		TestUtil.sleepTime(1500);		
	//	}

	public static void no_element_err() {
		TestUtil.sleepTime(1000);
		if (driver.findElements(By.id("back_arrow")).size()==1) {
			driver.findElement(By.id("back_arrow")).click(); 
			TestUtil.visibilityEleWaitByLocator(By.id("txt_kyat")); 
		} else if (driver.findElements(By.id("btn_error_try_again")).size()==1) {
			driver.findElement(By.id("btn_error_try_again")).click();
			TestUtil.sleepTime(2000);
			if (driver.findElements(By.xpath("//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]")).size()==1) {
				driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]")).click();
			}
			else if(driver.findElements(By.id("back_arrow")).size()==1) {driver.findElement(By.id("back_arrow")).click(); }
			else {driver.findElement(By.xpath("(//android.widget.ImageView[@content-desc=\"WC PreProd\"])[1]")).click();}
			//			driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]")).click();
			TestUtil.visibilityEleWaitByLocator(By.id("txt_kyat")); 
		} else if (driver.findElements(By.xpath("//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]")).size()==1) {
			driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‏‏‏‏‏‎‏‎‎‎‏‏‎‏‎‎‎‏‏‎‎‎‏‏‏‏‎‏‎‎‎‎‏‏‎‏‏‎‏‎‎‏‎‎‏‎‎‎‎‎‎‏‎‏‎‎‎‎‏‏‏‎‎‎‎‎Navigate up‎‏‎‎‏‎\"]")).click();
			TestUtil.visibilityEleWaitByLocator(By.id("txt_kyat")); 
		}
	}

	public static String getRandomNumberString() {
		// It will generate 6 digit random Number.
		// from 0 to 999999
		Random rnd = new Random();
		int number = rnd.nextInt(999999);

		// this will convert any number sequence into 6 character.
		return String.format("%06d", number);
	}


	//	@SuppressWarnings("rawtypes")
	//	public static void swipe(int startX,int startY, int endX, int endY){
	//		try {
	//			TouchAction touchAction = new TouchAction(driver);
	//			PointOption pointStart = PointOption.point(startX, startY);
	//			PointOption pointEnd = PointOption.point(endX, endY);
	//			WaitOptions waitOption = WaitOptions.waitOptions(Duration.ofMillis(1000));
	//			touchAction.press(pointStart).waitAction(waitOption).moveTo(pointEnd).release().perform();
	//		}catch (Exception e){
	//			e.printStackTrace();
	//		}
	//	}





}
