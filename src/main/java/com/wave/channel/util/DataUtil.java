package com.wave.channel.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.wave.channel.base.TestBase;

public class DataUtil extends TestBase {
	
	static String jsonContent ;
	public static String get_Data() {		
		FileReader fr;
		try {
			fr = new FileReader("./src/main/java/com/wave/channel/testdata/Data.json");
			BufferedReader br = new BufferedReader(fr);
			String line = null ;
			StringBuilder sb = new StringBuilder();
			while((line=br.readLine())!=null) {
				sb.append(line);
			}
			br.close();
			fr.close();
			jsonContent = sb.toString();			

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonContent;
	}
	
	public static String get_Error() {		
		FileReader fr;
		try {
			fr = new FileReader("./src/main/java/com/wave/channel/testdata/ErrorMsg.json");
			BufferedReader br = new BufferedReader(fr);
			String line = null ;
			StringBuilder sb = new StringBuilder();
			while((line=br.readLine())!=null) {
				sb.append(line);
			}
			br.close();
			fr.close();
			jsonContent = sb.toString();			

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonContent;
	}
	
//	@SuppressWarnings("unchecked")
//	public static void wirteTID(String parent,String label,String value) {
//		
//		 JSONObject output = new JSONObject();
//		 output.put(label, value);
//		 
//		 JSONObject resultData = new JSONObject(); 
//		 resultData.put(parent, output);
//		 
//		 try (FileWriter file = new FileWriter("./src/main/java/com/wave/channel/testdata/Result.json")) {
//	            //We can write any JSONArray or JSONObject instance to the file
//	            file.write(resultData.toJSONString()); 
//	            file.flush();
//	 
//	        } catch (IOException e) {
//	            e.printStackTrace();
//	        }
//	}
//	
//	public static String readTID() {		
//		FileReader fr;
//		try {
//			fr = new FileReader("./src/main/java/com/wave/channel/testdata/Result.json");
//
//			BufferedReader br = new BufferedReader(fr);
//			String line = null ;
//			StringBuilder sb = new StringBuilder();
//			while((line=br.readLine())!=null) {
//				sb.append(line);
//			}
//			br.close();
//			fr.close();
//			jsonContent = sb.toString();			
//
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return jsonContent;
//	}
//	
//	@SuppressWarnings("unchecked")
//	public static void wirteBalance(String beforeBal,String amt,String afterBal,String fee,String comm) {
//		
//		 JSONObject output = new JSONObject();
//		 output.put("BeforeBalance", beforeBal);
//		 output.put("Amount", amt);
//		 output.put("Fee", fee);
//		 output.put("Commission", comm);
//		 output.put("AfterBalance", afterBal);
//		 
//		 
//		 JSONObject resultData = new JSONObject(); 
//		 resultData.put("BalanceDeduction", output);
//		 
//		 try (FileWriter file = new FileWriter("./src/main/java/com/wave/channel/testdata/Temp.json")) {
//	            //We can write any JSONArray or JSONObject instance to the file
//	            file.write(resultData.toJSONString()); 
//	            file.flush();
//	 
//	        } catch (IOException e) {
//	            e.printStackTrace();
//	        }
//	}
//	
//	public static String readBalance() {		
//		FileReader fr;
//		try {
//			fr = new FileReader("./src/main/java/com/wave/channel/testdata/Temp.json");
//
//			BufferedReader br = new BufferedReader(fr);
//			String line = null ;
//			StringBuilder sb = new StringBuilder();
//			while((line=br.readLine())!=null) {
//				sb.append(line);
//			}
//			br.close();
//			fr.close();
//			jsonContent = sb.toString();			
//
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return jsonContent;
//	}
	
	

}
