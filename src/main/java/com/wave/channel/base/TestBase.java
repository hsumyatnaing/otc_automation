package com.wave.channel.base;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestListener;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class TestBase implements ITestListener {

	public static AppiumDriver<MobileElement> driver; 
	public static DesiredCapabilities caps;
	public static Properties prop;
	public static ExtentTest test;
	public static String dest;
	
	public static String tid_SendMoney2OTC;
	public static String secretCode_OTC ;

	protected static final Logger logger = LogManager.getLogger(TestBase.class);
	
//	public SoftAssert softassert = new SoftAssert();

	public TestBase() {	}

	public static void fileConfig() {
		try {
			prop = new Properties();
			FileInputStream file = new FileInputStream(System.getProperty("user.dir")+"/src/main/java/com/wave/channel/config/configs.properties");
			prop.load(file);
		} catch (FileNotFoundException e) {	
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void initialize() {	
		fileConfig();
		if (prop.getProperty("automationname").equals("UiAutomator2")) // need to modify 
		{
			try {
				caps = new DesiredCapabilities();
				caps.setCapability("automationName", prop.getProperty("automationname"));
				caps.setCapability("platformName", prop.getProperty("platformname"));
				caps.setCapability("platformVersion", prop.getProperty("platformversion"));
				caps.setCapability("udid", prop.getProperty("udid"));
				caps.setCapability("newCommandTimeout", prop.getProperty("newcommandtimeout"));
				caps.setCapability("appPackage", prop.getProperty("apppackage"));
				caps.setCapability("appActivity", prop.getProperty("appactivity"));
                caps.setCapability("autoGrantPermissions", "true");	

				driver = new AndroidDriver<MobileElement>(new URL(prop.getProperty("serverurl")),caps);	

			} catch (MalformedURLException e) {				
				e.printStackTrace();
			}

		} else if (prop.getProperty("automationname").equals("XCUITest")) {			
			System.out.println("Are you kidding me ?? I don't have iOS device.");
		} else {
			System.out.println("Hey, Come on ! Try either Android or iOS.");
		}

	}
	


}
