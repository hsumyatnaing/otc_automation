﻿## WaveChannel Automation Project

This project is intended to test the regression test cases of  WaveChannel application effectively without costing many human resources and time.  

This project is created by using Appium mobile test automation framework with Page Object Model design using Java + Maven + TestNG on read android device.

## Setup
1. Install Android Studio 
2. Set Environment variables for Android
3. Java Development Kit (JDK)
4. Set Environment variables for Java
5. Install Eclipse IDE
6. Install Appium Desktop

## Prerequisites
1. Install Wave Channel (UAT) App > Version 3.2.0
2. Connect Android device via USB 
3. Open command Prompt and find *udid* by command > ***adb devices***
4. Copy and Replace *udid* in **configs.properties**
5. Change *MSISDN*, *Pin* and *Platformversion* in **configs.properties**
6. Start Appium Server 
7. Copy and Replace *server port* in **configs.properties**
Note : If you want to run Version 3.1.0 , you need to change *apppackage* to `mm.com.wavemoney.mfs.wavepartner.preprod` 


> How to find apppackage name in Window
>  1. Connect your Android device
> and open the app 
> 2. Open a Command Prompt 
> 3. Enter cmd adb devices to
> see the connected devices 
> 4. Enter cmd adb shell dumpsys window | find
> "mCurrentFocus" 
> **The part before the '/' character is the Package
> name and the part after that is the Activity name.**

## Files and folders
1. **Log in** information can be changed in config.properties file where it is  *src\main\java\com\wave\channel\config\configs.properties*
2. **Pages** are under *com.wave.channel.pages* folder
3. **Test scripts** are under *com.wave.channel.testcases* folder
4. **Report** can be checked under *Reports* folder
	*Reports are created by yyyyMMddHHmm format. 
	Navigate to folder location and open .html file with browser for better view.*
5. **Log** can be checked under *\logs* folder
6. **Biller test data** can be found in *\src\test\resources*


## Run different test cases
1.  Build the project and wait until all necessaries are downloaded
2. Add the classes that you want to run in testng.xml file as per below example

    		<class name="com.wave.channel.testcases.LogInTest" ></class>	
			<class name="com.wave.channel.testcases.BillersTest"> </class>	
3. Right click on .xml file and RunAs > TestNG Suite


## Output
1. For each successful transaction, **TID** can be found in the report.
2. For the test that is failed, **error** can be seen in the report.
3. **Screenshots** are already attached to the report and they can be found under *ScreenShots* folder
4.  ***Balance deduction*** is calculated for each payment transaction and the result is displayed in the report.

-------------------
**Step to setup the billers for automated testing**

We can test the following biller type

-   ONLINE
-   OFFLLINE

Note : We cannot automate test ONLINE_WEB biller type.  
There are two steps to add biller for automated biller testing.

1.  You need to create individual json file and put inside in data/billers folder
2.  Put the biller file name in data/biller_list.properties files



 **Creating json file for biller**

Here is the basic example of the json file for AEON

    {  
      "billerName":"AEON",
        "fieldsDetails": [
            {
              "key": "agreementId",
              "input-type": {
                "type": "number",
                "android-digits": "0123456789-"
              },
              "hint": "Agreement ID",
              "value":"1111-1-1111111111-1",
              "invalid-value":"000000",
              "validation-error-message": "You entered an invalid agreement ID."
            },
            {
              "key": "customerName",
              "input-type": {
                "type": "text"
              },
              "hint": "Customer Name",
              "value":"QA Engineer",
              "validation-error-message": "The name must be in English."
              
            },
            {
              "key": "amount",
              "input-type": {
                "type": "number",
                "android-digits": "0123456789"
              },
              "hint": "Amount",
              "value":"10",
              "validation-error-message": "You entered an invalid amount."
            },
            {
              "key": "info1",
              "input-type": {
                "type": "informationBox",
                "message": "Hello"
              }
            }
          ]
    }
  

There are two main field inside the biller JSON.

-   billerName: The name used to search in the search bar of the biller page
-   fieldsDetails: fields extracted from the middleware added with some value for testing purpose. The field should be in correct order as obtained from middleware.
- fieldsDetails must include 'key', 'hint' , 'value' , 'input-type' and 'validation-error-message' . 

Note : Just remove unnecessary fields when you take data from 'billerDetails.xml' and add the fields that need to run the test case (e.g. value). For the input type whose type is informationBox, we need to put empty string in the box so that these string cannot mess up in JSON. Other fields such as images are not needed. Beforing storing json in the system , you should validate the JSON with online tool such as [jsonviewer](http://jsonviewer.stack.hu/) so that JSON have proper format.

 **Format for the biller_list.properties**

Here is the example format for the biller_list.properties

    #mBuyy.json
    #aeon.json
    #maHarBawGaYangon.json

    5BB_BroadBand.json
    #comment example
    sunKingSolar.json
  

-   You need to put json file name with their extension (in that case .json)
-   These file should be under src/test/resources/data/billers folder.
-   You need put one json file per line
-   You can put blank line to seperate billers
-   You can use # to comment out the line, # must be the first character in the line




